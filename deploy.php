<?php

namespace Deployer;
require 'recipe/common.php';

// Configuration

set('ssh_type', 'native');
set('ssh_multiplexing', true);

set('repository', 'git@bitbucket.org:thinkfirsttechnologies/ogaranya-merchant.git');
set('shared_files', []);
set('shared_dirs', []);
set('writable_dirs', ['application/logs']);

// Servers

server('production', '139.59.187.74')
    ->user('root')
    ->identityFile()
    ->set('deploy_path', '/var/www/site')
    ->set('branch', 'master')
    ->forwardAgent();

task ('run_composer_update', function () {
    run('cd {{release_path}}/application && composer install');
});

task('move_environment_files', function() {
    run('cp /var/www/setup/.env {{deploy_path}}/current/application/config');
    run('cp /var/www/setup/database.php {{deploy_path}}/current/application/config');
    run('cp /var/www/setup/config.php {{deploy_path}}/current/application/config');
});


desc('Deploy Ogaranya');
task('deploy', [
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'run_composer_update',
    'deploy:clear_paths',
    'deploy:symlink',
    'move_environment_files',
    'deploy:unlock',
    'cleanup',
    'success'
]);

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');