<?php 

//Auth related
$lang['log_in']					= 'Log In';
$lang['register']				= 'Register';
$lang['sign_in']				= 'Sign In';


$lang['dashboard']				= 'Dashboard';


//products
$lang['products']				= 'Products';
$lang['product']				= 'Product';
$lang['price']					= 'Price';
$lang['donations']				= 'Donations';
$lang['product_module']			= 'Products';
$lang['product_form']			= 'Product Form';
$lang['product_name']			= 'Product Name';
$lang['product_price']			= 'Product Price';
$lang['descritpion']			= 'Description';

//Events
$lang['events']					= 'Events';
$lang['event_module']			= 'Events';

//Subscribers
$lang['subscribers'] 			= 'Subscribers';

//Orders
$lang['orders']					= 'Orders';

//Wallet
$lang['wallet']					= 'Wallet';

//Customers
$lang['customers']				= 'Customers';

//Account Settings
$lang['account_settings']		= 'Account Settings';

//Reports
$lang['reports']				= 'Reports';


//Forms
$lang['add_new']				= 'Add New';
$lang['save']					= 'Save';
$lang['close']					= 'Close';
$lang['upload_bulk']			= 'Upload Bulk';

//Tables
$lang['code']					= 'Code';
$lang['msisdn']					= 'Phone';
$lang['name']					= 'Name';
$lang['quantity']				= 'Quantity';
$lang['action']					= 'Action';

//Others
$lang['action_succesful']		= 'Action Successful';
$lang['action_unsuccesful']		= 'Action Unsuccessful';
