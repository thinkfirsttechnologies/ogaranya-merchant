<?php
class Datatable
{
    function __construct()
    {
        $this->obj =& get_instance();
    }
    //--------------------------------------------
    function LoadJson($SQL,$EXTRA_WHERE='',$GROUP_BY='')
    {
        if(!empty($EXTRA_WHERE))
        {
            $SQL .= " WHERE ( $EXTRA_WHERE )";
        }
        else
        {
            $SQL .= " WHERE (1)";
        }
        //$query = $this->obj->db->query($SQL);
        //$Total = $query->num_rows();
        //------------------------------------------------
        if(!empty($_GET['search']['value']))
        {
            $qry = array();

            foreach($_GET['columns'] as $cl)
            {
                if($cl['searchable']=='true' && $cl['name'] != '' )
                    $qry[] = " " . $cl['name'] . " LIKE '%" . $_GET['search']['value'] . "%' ";
            }

            $SQL .= "AND ( ";
            $SQL .= implode("OR",$qry);
            $SQL .= " ) ";
        }

        //------------------------------------------------
        if($GROUP_BY != '')
        {
            $SQL .= $GROUP_BY;
        }
        //------------------------------------------------
        $query = $this->obj->db->query($SQL);
        $Filtered = $query->num_rows();
        $Total = $Filtered;
        $len = $_GET['length'] == -1 ?  $Filtered : $_GET['length'];
        $_start =  $_GET['start'] == -1 ? 0 : $_GET['start'];

        $SQL.= " ORDER BY ";
        $SQL.= $_GET['columns'][$_GET['order'][0]['column']]['name']." ";
        $SQL.= $_GET['order'][0]['dir'];
        $SQL.= " LIMIT " .$len . " OFFSET ".$_start." ";
        $query = $this->obj->db->query($SQL);
        $Data = $query->result_array();


        return array("recordsTotal"=>$Total,"recordsFiltered"=>$Filtered,'data' => $Data);
    }






    function LoadJson2($SQL,$EXTRA_WHERE='', $EXTRA_WHERE2='', $GROUP_BY='')
    {
        if(!empty($EXTRA_WHERE))
        {
            $SQL .= " WHERE ( $EXTRA_WHERE )";
        }
        else
        {
            $SQL .= " WHERE (1)";
        }
        //$query = $this->obj->db->query($SQL);
        //$Total = $query->num_rows();
        //------------------------------------------------

        if(!empty($EXTRA_WHERE2))
        {
            $SQL .= " AND " .$EXTRA_WHERE2 ;
        }

        //$query = $this->obj->db->query($SQL);
        //$Total = $query->num_rows();
        //------------------------------------------------

        if(!empty($_GET['search']['value']))
        {
            $qry = array();

            foreach($_GET['columns'] as $cl)
            {
                if($cl['searchable']=='true' && $cl['name'] != '' )
                    $qry[] = " " . $cl['name'] . " LIKE '%" . $_GET['search']['value'] . "%' ";
            }

            $SQL .= "AND ( ";
            $SQL .= implode("OR",$qry);
            $SQL .= " ) ";
        }

        //------------------------------------------------
        if($GROUP_BY != '')
        {
            $SQL .= $GROUP_BY;
        }
        //------------------------------------------------
        $query = $this->obj->db->query($SQL);
        $Filtered = $query->num_rows();
        $Total = $Filtered;

        $SQL.= " ORDER BY ";
        $SQL.= $_GET['columns'][$_GET['order'][0]['column']]['name']." ";
        $SQL.= $_GET['order'][0]['dir'];
        $SQL.= " LIMIT " .$_GET['length'] . " OFFSET ".$_GET['start']." ";
        $query = $this->obj->db->query($SQL);
        $Data = $query->result_array();

        return array("recordsTotal"=>$Total,"recordsFiltered"=>$Filtered,'data' => $Data);
    }












}
?>