 <?php
defined('BASEPATH') or exit('No direct script access allowed');

class Notification
{

    static function sendEmail($email = '', $subject = '', $body = '', $file = '', $cc = '')
    {


        $api_key = "key-f9c101361bccf892331187a1f07120dc";/* Api Key got from https://mailgun.com/cp/my_account */
        $domain = "mg.ogaranya.com";//Domain Name you given to Mailgun

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, 'api:' . $api_key);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v3/' . $domain . '/messages');


        $mail_array = [];

        $mail_array['from'] = 'Ogaranya <no-reply@ogaranya.com>';
        $mail_array['to'] = $email;
        $mail_array['subject'] = $subject;
        $mail_array['html'] = $body;
        $mail_array['bcc'] = 'think@ogaranya.com';

        if ($cc != '') {
            $mail_array['to'] = $cc;
        }

        if (empty($file)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $mail_array);
        } else {

            $mail_array['attachment'] = curl_file_create(FCPATH . "uploads/" . $file, 'application/pdf');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $mail_array);
        }

        $result = curl_exec($ch);
        curl_close($ch);
        return   $result;
    }
}
