  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Withdrawal Requests</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
          <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li class="active">Withdrawal Requests</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- /row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <h3 class="box-title m-b-0">Withdrawal Requests</h3>
            <div class="table-responsive">
              <?php $this->load->view('includes/alerts') ?>
              <table id="datatable" class="table table-striped">
                <thead>
                  <tr>
                    <th>&nbsp;</th>
                    <th>Request Date</th>
                    <th><?=l('merchant') ?></th>
                    <th>Withdrwal Request</th>
                    <th>status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $count = 1; ?>
                  <?php if($withdrawals): ?>
                    <?php foreach($withdrawals as $row): ?>
                      <tr>
                        <td><?=$count++ ?></td>
                        <td><?=d($row->created_at) ?></td>
                        <td><?=DB::first(MERCHANTS, ['id'=>$row->merchant_id])->merchant_name ?></td>
                        <td><?=c($row->amount) ?></td>
                        <td><?=$row->status ?></td>
                        <td>
                          <?=form_open('admin/withdrawal_approve/'.md5(time())) ?>
                          <input type="hidden" name="withdrawal_id" value="<?=$row->withdrawal_id ?>">
                          <input type="submit" value="Approve" class="btn btn-success" onclick="return confirm('Are you absolutely sure you want to approve this withdrawal request? - This operation cannot be undone.')">
                          <?=form_close() ?>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </tbody>
                </table
              </div>
            </div>
          </div>

        </div>
        <!-- /.row -->

      </div>
      <!-- /.container-fluid -->
      <?php $this->load->view('includes/footer') ?>
    </div>
    <!-- /#page-wrapper -->
  </div>