   <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Merchant Form</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
          <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li class="active">Merchant</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- /row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <h3 class="box-title m-b-0">Merchant Form</h3>
            
            <div role="tabpanel">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">

                <li role="presentation" class="active">
                  <a href="#tab" aria-controls="tab" role="tab" data-toggle="tab">Merchant</a>
                </li>
                
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab">
                  <?php $this->load->view('includes/alerts') ?>
                  <?=form_open('admin/new_merchant/'.md5(time().time())) ?>
                  
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Merchant Name</label>
                        <input type="text" class="form-control" name="merchant_name" placeholder="Merchant / Shop Name" required value="<?=set_value('merchant_name') ?>">
                        <span class="form-error"><?=form_error('merchant_name') ?></span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Merchant Phone</label>
                        <input type="text" class="form-control" name="merchant_phone" placeholder="00000000000" required value="<?=set_value('merchant_phone') ?>">
                        <span class="form-error"><?=form_error('merchant_phone') ?></span>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Merchant Email</label>
                        <input type="text" class="form-control" name="merchant_email" placeholder="valid email address" required value="<?=set_value('merchant_email') ?>">
                        <span class="form-error"><?=form_error('merchant_email') ?></span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Merchant Address</label>
                        <textarea name="merchant_address" class="form-control"  required>
                          <?=set_value('merchant_address') ?>
                        </textarea>
                        <span class="form-error"><?=form_error('merchant_address') ?></span>
                      </div>
                    </div>
                  </div> 

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Merchant Contact Person</label>
                        <input type="text" class="form-control" name="merchant_contact_person" required value="<?=set_value('merchant_contact_person') ?>">
                        <span class="form-error"><?=form_error('merchant_contact_person') ?></span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="">Merchant Country</label>
                        <select name="country_id" class="form-control" required="">
                          <option value="">--select--</option>
                          <?php foreach(DB::get(COUNTRIES) as $row): ?>
                            <option value="<?=$row->id ?>"><?=$row->country_name ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                  
                  <?=form_close() ?>
                </div>
              </div>
            </div>
          </div>

        </div>
        <!-- /.row -->

      </div>
      <!-- /.container-fluid -->
      <?php $this->load->view('includes/footer') ?>
    </div>
  </div>