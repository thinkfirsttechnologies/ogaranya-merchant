  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Parent Merchants</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
          <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li class="active">Parent Merchants</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- /row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <h3 class="box-title m-b-0">Parent Merchants Module</h3>
            <a href="<?=site_url('admin/new_parent_merchant') ?>" class="btn btn-info pull-right">
                <?=l('add_new') ?>
              </a>
            <br><br>
            <div class="table-responsive">
            <?php $this->load->view('includes/alerts') ?>
              <table id="datatable" class="table table-striped">
                <thead>
                  <tr>
                    <th>&nbsp;</th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Children</th>
                    <th>Last Login</th>
                    <th>Actions</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>

                  <?php $count = 1; ?>
                  <?php if(count($merchants) > 0): ?>
                    <?php foreach($merchants as $row): ?>
                      <tr>
                        <td><?=$count++ ?></td>
                        <td><?=ucwords($row->merchant_name) ?></td>
                        <td><?=ucwords($row->merchant_phone) ?></td>
                        <td><?=ucwords($row->merchant_email) ?></td>
                        <td><?= DB::numRows(MERCHANTS,  ['parent_id' =>$row->id]) ?></td>
                        <td><?=d($row->last_login) ?></td>
                        <td>
                          <div class="btn-group m-r-10 btn-sm">
                            <button aria-expanded="true" data-toggle="dropdown" class="btn btn-info dropdown-toggle waves-effect waves-light" type="button">Actions <span class="caret"></span></button>
                            <ul role="menu" class="dropdown-menu animated flipInX">
                              <?php if($row->status == ENABLED): ?>
                                <li>
                                  <a href="<?=site_url('admin/merchant_impersonate/'.$row->id) ?>">
                                    Impersonate
                                  </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                  <a href="<?=site_url('admin/merchant_status/'.$row->id.'/'.DISABLED) ?>">
                                    Disable
                                  </a>
                                </li>
                              <?php else: ?>
                                <li>
                                  <a href="<?=site_url('admin/merchant_status/'.$row->id.'/'.ENABLED) ?>">
                                    Enable
                                  </a>
                                </li>
                              <?php endif; ?>

                            </ul>
                          </div>
                        </td>
                        <td><?=ucwords($row->status) ?></td>
                      </tr>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </tbody>
                </table
              </div>
            </div>
          </div>

        </div>
        <!-- /.row -->

      </div>
      <!-- /.container-fluid -->
      <?php $this->load->view('includes/footer') ?>
    </div>
    <!-- /#page-wrapper -->
  </div>
