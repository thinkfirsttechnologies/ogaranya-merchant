  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Agents</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
          <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li class="active">Agents</li>
          </ol>
        </div>
        
      </div>
      
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <h3 class="box-title m-b-0">Agents</h3>
            <div class="table-responsive">
            <?php $this->load->view('includes/alerts') ?>
              <table id="datatable" class="table table-striped table-responsive">
                <thead>
                  <tr>
                    <th>S/NO.</th>
                    <th>Date Added</th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Commission</th>
                    <th>Actions</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>

                  <?php $count = 1; ?>
                  <?php if(count($agents) > 0): ?>
                    <?php foreach($agents as $row): ?>
                      <tr>
                        <td><?=$count++ ?></td>
                        <td><?=d($row->date_added) ?></td>
                        <td><?=ucwords($row->agent_name) ?></td>
                        <td><?=ucwords($row->agent_phone) ?></td>
                        <td><?=ucwords($row->agent_email) ?></td>
                        <td><?=c($row->wallet) ?></td>
                        <td>
                          <div class="btn-group m-r-10 btn-sm">
                            <button aria-expanded="true" data-toggle="dropdown" class="btn btn-info btn-xs dropdown-toggle waves-effect waves-light" type="button">Actions <span class="caret"></span></button>
                            <ul role="menu" class="dropdown-menu animated flipInX">
                              <?php if($row->status == ENABLED): ?>
                                <li>
                                  <a href="<?=site_url('admin/agent_impersonate/'.$row->id) ?>">
                                    Impersonate
                                  </a>
                                </li>
                                <li>
                                  <a href="<?=site_url('admin/agent_password/'.$row->id) ?>">
                                    Change Password
                                  </a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                  <a href="<?=site_url('admin/agent_status/'.$row->id.'/'.DISABLED) ?>">
                                    Disable
                                  </a>
                                </li>
                              <?php else: ?>
                                <li>
                                  <a href="<?=site_url('admin/agent_status/'.$row->id.'/'.ENABLED) ?>">
                                    Enable
                                  </a>
                                </li>
                              <?php endif; ?>

                            </ul>
                          </div>
                        </td>
                        <td><?=badge($row->status) ?></td>
                      </tr>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </tbody>
                </table
              </div>
            </div>
          </div>

        </div>
        

      </div>
      
      <?php $this->load->view('includes/footer') ?>
    </div>
    
  </div>

  <div class="modal fade" id="generateCode">
    <div class="modal-dialog">
      <?=form_open('admin/generate_codes') ?>
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Generate Code</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="">How many codes do you want to generate?</label>
            <input type="number" name="count" class="form-control" min="1" required minlength="1" placeholder="0">
            <input type="hidden" name="agent_id" id="agent_id">
            <small class="text-danger">On generating codes, please note that the codes would be sent as a mail to the respective agent.</small>
          </div>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Generate</button>
        </div>
      </div>
      <?=form_close() ?>
    </div>
  </div>

  <script>
    
    function launchGenerateCodeModal(agent_id)
    {
      $('#generateCode').modal();
      $('#agent_id').val(agent_id);
    }
  </script>