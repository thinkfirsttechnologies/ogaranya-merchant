   <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Set Password for <?=$agent->agent_name ?></h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
          <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Agent</a></li>
            <li class="active"><?=$agent->agent_name ?></li>
          </ol>
        </div>
        
      </div>
      
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <h3 class="box-title m-b-0">Change Password</h3>
            
            <div role="tabpanel">
              
              <ul class="nav nav-tabs" role="tablist">

                <li role="presentation" class="active">
                  <a href="#tab" aria-controls="tab" role="tab" data-toggle="tab"><?=$agent->agent_name ?></a>
                </li>
                
              </ul>

              
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab">

                  <?php $this->load->view('includes/alerts') ?>

                  <?=form_open('admin/agent_password/'.$agent->id) ?>
                  
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>New Password</label>
                        <input type="password" class="form-control" name="password" placeholder="New Password" required value="<?=set_value('password') ?>">
                        <span class="form-error"><?=form_error('password') ?></span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Retype New Password</label>
                        <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
                        <span class="form-error"><?=form_error('password_confirmation') ?></span>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                  
                  <?=form_close() ?>
                </div>
              </div>
            </div>
          </div>

        </div>
        

      </div>
      
      <?php $this->load->view('includes/footer') ?>
    </div>
  </div>