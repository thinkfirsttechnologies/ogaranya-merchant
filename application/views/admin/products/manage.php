  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">All Products</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
          <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li class="active">All Products</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- /row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <h3 class="box-title m-b-0">Products Module</h3>
            <div class="table-responsive">
              <?php $this->load->view('includes/alerts') ?>
              <table id="datatable" class="table table-striped">
                <thead>
                  <tr>
                    <th>&nbsp;</th>
                    <th><?=l('code') ?></th>
                    <th><?=l('merchant') ?></th>
                    <th><?=l('name') ?></th>
                    <th><?=l('price') ?></th>
                    <th><?=l('qty') ?></th>
                    <th><?=l('type') ?></th>
                  </tr>
                </thead>
                <tbody>
                  <?php $count = 1; ?>
                  <?php if($products): ?>
                    <?php foreach($products as $row): ?>
                      <tr>
                        <td><?=$count++ ?></td>
                        <td><?=$row->product_code ?></td>
                        <td><?=DB::getCell(MERCHANTS, ['id'=>$row->merchant_id], 'merchant_name') ?></td>
                        <td><?=$row->product_name ?></td>
                        <td><?=c($row->product_price) ?></td>
                        <td><?=$row->quantity ?></td>
                        <td><?=$row->product_type ?></td>
                      </tr>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </tbody>
                </table
              </div>
            </div>
          </div>

        </div>
        <!-- /.row -->

      </div>
      <!-- /.container-fluid -->
      <?php $this->load->view('includes/footer') ?>
    </div>
    <!-- /#page-wrapper -->
  </div>