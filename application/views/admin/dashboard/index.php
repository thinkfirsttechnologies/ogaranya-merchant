   
   <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Administrator Dashboard</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
          <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Administrator Dashboard</li>
          </ol>
        </div>
        
      </div>
      
      <div class="row">
        <div class="col-lg-12">
          <h3>Merchants</h3>
        </div>

        <div class="col-lg-3 col-sm-3 col-xs-12">
          <div class="white-box analytics-info">
            <h3 class="box-title">Merchants</h3>
            <ul class="list-inline two-part">
              <li>
                <div id="sparklinedashss"></div>
              </li>
              <li class="text-right"><span class="counter text-success"><?=number_format($totalMerchants) ?></span></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-sm-3 col-xs-12">
          <div class="white-box analytics-info">
            <h3 class="box-title">Completed Transactions</h3>
            <ul class="list-inline two-part">
              <li>
                <div id="sparklinedashss2"></div>
              </li>
              <li class="text-right"><span class="counter text-purple"><?=number_format($transCount) ?></span></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-sm-3 col-xs-12">
          <div class="white-box analytics-info">
            <h3 class="box-title">Pending W. Requests</h3>
            <ul class="list-inline two-part">
              <li>
                <div id="sparklinedasssh3"></div>
              </li>
              <li class="text-right"><span class="counter text-info"><?=number_format($pendingRequests) ?></span></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-sm-3 col-xs-12">
          <div class="white-box analytics-info">
            <h3 class="box-title"> Total Unique Customers</h3>
            <ul class="list-inline two-part">
              <li>
                <div id="sparklinessdash4"></div>
              </li>
              <li class="text-right"><span class="counter text-info"><?=number_format($uniqueCustomers) ?></span></li>
            </ul>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12">
          <h3>Agents</h3>
        </div>
        <div class="col-lg-3 col-sm-3 col-xs-12">
          <div class="white-box analytics-info">
            <h3 class="box-title">Agents</h3>
            <ul class="list-inline two-part">
              <li>
                <div id="sparklinssedash"></div>
              </li>
              <li class="text-right"><span class="counter text-success"><?=number_format(DB::numRows('agents')) ?></span></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-sm-3 col-xs-12">
          <div class="white-box analytics-info">
            <h3 class="box-title">Completed Transactions</h3>
            <ul class="list-inline two-part">
              <li>
                <div id="sparklissnedash2"></div>
              </li>
              <li class="text-right"><span class="counter text-purple"><?=number_format(DB::count('digital_order_fulfilment', ['status' => 'completed'])) ?></span></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-sm-3 col-xs-12">
          <div class="white-box analytics-info">
            <h3 class="box-title">Completed Trans. Total</h3>
            <ul class="list-inline two-part">
              <li>
                <div id="sparklinedasssh3"></div>
              </li>
              <li class="text-right"><span class="counter text-info"><?=number_format(DB::tableSum('digital_order_fulfilment', 'amount', ['status' => 'completed'])) ?></span></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-sm-3 col-xs-12">
          <div class="white-box analytics-info">
            <h3 class="box-title">Commission</h3>
            <ul class="list-inline two-part">
              <li>
                <div id="sparklinssedash4"></div>
              </li>
              <li class="text-right"><span class="counter text-info"><?=number_format(DB::tableSum('agents', 'wallet')) ?></span></li>
            </ul>
          </div>
        </div>
      </div>
      
      
      <div class="row">
        <div class="col-md-12">
          <div class="white-box">
            <div class="row">
              <div class="col-md-7 col-sm-12 col-xs-12">
                <div class="white-box">
                  <h3 class="box-title">Activity Log</h3>
                  <div class="steamline">
                    <?php foreach($userActivity as $row): ?>
                      <div class="sl-item">
                        <div class="sl-left"> 
                          <img class="img-circle" alt="user" src="<?=base_url() ?>assets/logo.png"> 
                        </div>
                        <div class="sl-right">
                          <div>
                            <a href="#">Admin</a> 
                            <span class="sl-date">
                              <?=date('Y-m-d g:i A', strtotime($row->created_at)) ?>
                            </span>
                          </div>
                          <p><?=$row->activity ?></p>
                        </div>
                      </div>
                    <?php endforeach; ?>
                  </div>
                </div>
              </div>
             
               
              </div>
            </div>
          </div>
        </div>
      </div>
      
      
    </div>
    
    <?php $this->load->view('includes/footer') ?>
  </div>