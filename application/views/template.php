<?php $this->load->view('includes/head') ?>
<body>
	<?php $url  = s('merchant_status') == DISABLED ? false : true; ?>

	<nav class="navbar navbar-info navbar-static-top m-b-0">
		<?php if(!$url): ?>
			<div class="top-disabled">Your account is currently inactive. For more information pls call 0808 243 1008</div>
		<?php endif ?>
<!--		<div class="preloader">-->
<!--			<div class="cssload-speeding-wheel"></div>-->
<!--		</div>-->
		
		<div id="wrapper">

			<?php $this->load->view('includes/nav-top') ?>

			<?php if($this->session->has_userdata('admin_id')): ?>

				<?php $this->load->view('includes/nav-left-admin') ?>

			<?php elseif($this->session->has_userdata('agent_id')): ?>

				<?php $this->load->view('includes/nav-left-agent') ?>

			<?php else: ?>

				<?php $this->load->view('includes/nav-left') ?>

			<?php endif ?>
			
			<?php if(isset($page)) $this->load->view($page) ?>

		</div>

		<?php  isset($products) ? $this->load->view('includes/scripts', $products) : $this->load->view('includes/scripts') ?>
	</body>
	</html>
