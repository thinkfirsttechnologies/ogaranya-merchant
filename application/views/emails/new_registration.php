
<table width="90%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center">
			<div style="height: 60px; line-height: 60px; font-size: 10px;"> </div>
			<div style="line-height: 44px;">
				<font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
					<span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
						Thank you for your interest in Ogaranya
					</span>
				</font>
			</div>
			<div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
		</td>
	</tr>
	<tr>
		<td align="justify">
			<div style="line-height: 24px;">
				<font face="Arial, Helvetica, sans-serif" size="4" color="#57697e" style="font-size: 15px;">
					<span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">
						Dear <strong><?=$merchant->merchant_name ?></strong>, <br><br>

						This is Akinyele Olubodun, the founder of Ogaranya, it gives me great pleasure to welcome you to the world of convenient commerce.<br><br>
						
						The commerce value chain is broken and every attempt to fix it has resulted in many jagged edges of pain; especially the pain of ineffective payment systems. Sigh! We know this pain and more.<br><br>

						We built Ogaranya purposely to fix this problem for enterprise like yours and we are excited about the system that we have built. We know that each business is peculiar in its operations, but all organisations need to be able to collect fund effectively and efficiently to stay alive.<br><br>

						We would like to know what your expectations are and if you have other payment pains that you want to resolve we would like to hear from you. We pledge to give your business a leverage that is not available anywhere else. <br><br>

						Please <a href="<?=site_url('site/form') ?>" target="_blank">click here</a> to download your merchant onboarding form.<br><br>

						I enjoin you to quickly complete this form and return them to solutions@ogaranya.com. If you need any additional support, please call <strong>0808 243 1008</strong><br><br>

						Once again, I welcome you to the world of commerce as it was meant to be; EASY!<br><br>

						Thank You.
					</span>
				</font>
			</div>
			<div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
		</td>
	</tr>
</table>