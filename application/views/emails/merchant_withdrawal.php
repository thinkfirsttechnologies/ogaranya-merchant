
<table width="90%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center">
			<div style="height: 60px; line-height: 60px; font-size: 10px;"> </div>
			<div style="line-height: 44px;">
				<font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
					<span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
						Withdrawal Request Notification
					</span>
				</font>
			</div>
			<div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
		</td>
	</tr>
	<tr>
		<td align="justify">
			<div style="line-height: 24px;">
				<font face="Arial, Helvetica, sans-serif" size="4" color="#57697e" style="font-size: 15px;">
					<span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">
						Dear <strong><?=$merchant->merchant_name ?></strong>, <br><br>
						Your Withdrawal request has been recieved. <br><br>
						Please note this request is being attended to. Expect a feedback before the end of the next business day.
					</span>
				</font>
			</div>
			<div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
		</td>
	</tr>
</table>