
<table width="90%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center">
			<div style="height: 60px; line-height: 60px; font-size: 10px;"> </div>
			<div style="line-height: 44px;">
				<font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
					<span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
						New Account Notification
					</span>
				</font>
			</div>
			<div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
		</td>
	</tr>
	<tr>
		<td align="justify">
			<div style="line-height: 24px;">
				<font face="Arial, Helvetica, sans-serif" size="4" color="#57697e" style="font-size: 15px;">
					<span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">
						Dear <strong><?=$merchant_name ?></strong>, <br><br>
						Your request to register as a Merchant on our platform has been approved an acount has been
						created for you. We are excited to have you on board. Please tell others about this platform.<br><br>
						Please find details of your account information below.<br><br>

						<strong>Email: <?=$merchant_email ?></strong><br>
						<strong>Password: <?=$merchant_password ?></strong><br><br>

						You are advised to change this password as soon as possible. <a href="<?=site_url('authentication') ?>">Click here</a> to login to your acccount
					</span>
				</font>
			</div>
			<div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
		</td>
	</tr>
</table>