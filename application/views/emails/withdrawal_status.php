
<table width="90%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center">
			<div style="height: 60px; line-height: 60px; font-size: 10px;"> </div>
			<div style="line-height: 44px;">
				<font face="Arial, Helvetica, sans-serif" size="5" color="#57697e" style="font-size: 34px;">
					<span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">
						Withdrawal Status Notification
					</span>
				</font>
			</div>
			<div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
		</td>
	</tr>
	<tr>
		<td align="justify">
			<div style="line-height: 24px;">
				<font face="Arial, Helvetica, sans-serif" size="4" color="#57697e" style="font-size: 15px;">
					<span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">
						Dear <strong><?=$merchant->merchant_name ?></strong>, <br><br>

						Your withdrawal request of <strong><?=number_format($withdrawal->amount, 2) ?></strong> has been approved. Your current wallet balance is <strong><?=c($balance) ?></strong>.
						 
					</span>
				</font>
			</div>
			<div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
		</td>
	</tr>
</table>