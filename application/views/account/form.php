   <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Account Settings</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
          <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li class="active">Bank Account Details</li>
          </ol>
        </div>
        
      </div>
      
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <h3 class="box-title m-b-0">Bank Account Details</h3>
            
            <div role="tabpanel">

              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                  <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Bank Account Details</a>
                </li>

              </ul>

              
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">

                  <?=form_open('account/save/'.md5(time())) ?>
                  
                  <?php $this->load->view('includes/alerts') ?>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="control-label">Bank Name</label>

                        <?php if($merchant->country_id == 1): ?>
                          <select class="form-control" name="bank_name" id="bank_name">

                           <?php $r =1;  $wh = ['sort_code'=> 'IS NOT NULL'] ;
                           foreach(DB::get(BANKS,'name', 'ASC', 0,  0, 'sort_code', 'IS NOT NULL') as $row): ?>

                              <option value="<?=$row->name ?>" selected><?=$row->name.'='. $r?></option>

                            <?php $r++; endforeach ?>
                          </select>
                        <?php else: ?>
                          <input type="text" name="bank_name" required class="form-control">
                        <?php endif ?>

                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="" class="control-label">Account Number</label>
                        <input type="text" name="account_number" class="form-control" value="<?=set_value('account_number') ?>" required>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="" class="control-label">Account Name</label>
                        <input type="text" name="account_name" value="<?=set_value('account_name') ?>" class="form-control" required>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="" class="control-label">Bank Sort Code</label>
                        <input type="text" name="sort_code" class="form-control" id="sort_code">
                      </div>
                    </div>
                  </div>

                  <div class="row">

                   <div class="form-group col-md-6">
                    <label for="" class="control-label">Security Question</label>
                    <input type="text" name="security_question" class="form-control" required value="<?=set_value('security_question') ?>">
                  </div>

                  <div class="form-group col-md-6">
                    <label for="" class="control-label">Security Answer</label>
                    <input type="text" name="security_answer" class="form-control" required value="<?=set_value('security_answer') ?>">
                  </div>

                </div>

                <input type="hidden" name="country_id" value="<?=$merchant->country_id ?>">
                <button type="submit" class="btn btn-info">Submit</button>

                <?=form_close() ?>
              </div>

            </div>
          </div>
        </div>

      </div>


    </div>

    <?php $this->load->view('includes/footer') ?>

    <script type="text/javascript">

      $(() => {

        $('#bank_name').change(() => {

          $.post('<?=site_url('account/sort_code') ?>', {bank_name : $('#bank_name').val()}, response => {

            if(response.length > 1){

              $('#sort_code').val(response).attr('readonly', true);

            }else{

              $('#sort_code').val('').removeAttr('readonly');

            }


          });

        });

      })

    </script>
  </div>
</div>