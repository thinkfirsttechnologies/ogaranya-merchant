   <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Account Settings</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
          <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li class="active">Bank Account Details</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- /row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <h3 class="box-title m-b-0">Bank Account Details</h3>
            
            <div role="tabpanel">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                  <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Bank Account Details</a>
                </li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">
                  <?php $this->load->view('includes/alerts') ?>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="control-label">Bank Name</label>
                        <input type="text" class="form-control" value="<?=$account_details->bank_name ?>" readonly>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="control-label">Account Number</label>
                        <input type="text" name="account_number" class="form-control" value="<?=$account_details->account_number ?>" readonly>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="control-label">Account Name</label>
                        <input type="text" name="account_name" value="<?=$account_details->account_name ?>" class="form-control" readonly>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="control-label">Bank Sort Code</label>
                        <input type="text" name="sort_code" class="form-control" value="<?=$account_details->sort_code ?>" readonly>
                      </div>
                    </div>
                  </div>

                  <div class="well">
                   To request for account details modificaitons. Kindly send a mail to accounts@ogaranya.com
                 </div>
               </div>
             </div>
           </div>
         </div>

       </div>
       <!-- /.row -->

     </div>
     <!-- /.container-fluid -->
     <?php $this->load->view('includes/footer') ?>
   </div>
 </div>