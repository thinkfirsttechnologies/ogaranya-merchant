<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title"><?php if (isset($header)) echo $header; ?>(Test)</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Api Details</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->

        <div class="row">
            <div class="col-sm-12">
                <?php $this->load->view('includes/alerts') ?>
                <div class="white-box">
                    <h3 class="box-title m-b-0">Customer Api Details</h3>

                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Developers API
                                    keys</a>
                            </li>

                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <div class="table-responsive">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <?= form_open('developer/getApi/test') ?>

                                            <div class="row">
                                                <?php $token = isset($api->api_token) ? $api->api_token : ''; ?>
                                                <div class="form-group col-md-12">
                                                    <label>Merchant ID</label>
                                                    <input type="text" id="txt-field" class="form-control"
                                                           value="<?= $mid ?>">
                                                </div>

                                                <div class="form-group col-md-12">
                                                    <label>Token</label>
                                                    <input type="text" id="txt-field" class="form-control"
                                                           value="<?= $token ?>">
                                                    <input type="button"
                                                           value="Copy"
                                                           class="btn btn-primary btn-xs show-pass"

                                                           onclick="copyTxt('token')"/>
                                                </div>
                                                <?php $key = isset($api->private_key) ? $api->private_key : ''; ?>
                                                <div class="form-group col-sm-12">
                                                    <label><a href="#">Private key</a></label>
                                                    <input type="text" id="txt-field2" class="form-control"
                                                           value="<?= $key ?>">
                                                    <input type="button"

                                                           value="Copy" class="btn btn-primary btn-xs show-pass"
                                                           onclick="copyTxt('key')"/>

                                                </div>
                                            </div>


                                        </div>
                                        <div class="col-md-4 new-key-btn">

                                            <button type="submit" class="btn btn-transparent ">Generate new keys
                                            </button>
                                        </div>
                                        <?= form_close() ?>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

    <?php $this->load->view('includes/footer') ?>

</div>
<!-- /#page-wrapper -->
</div><?php
