<div class="w-section info--section"></div>
<div class="w-section s-info-holder">
    <div class="w-container">
        <div class="w-row">
            <div class="w-col w-col-8">
                <h1 class="support-heading-style">About Us</h1>
                <p class="p-text">
                    <strong>Ogaranya</strong> was conceptualized when a friend found it frustrating to get internet connection to buy a shirt during a prolonged fuel scarcity induced strike in Nigeria. We quickly thought up a solution and created Ogaranya.
                </p>
                <p class="p-text">
                    <strong>Ogaranya</strong> completes offline commerce transactions via text 
                    <strong>(SMS, Twitter, Telegram, Facebook, WeChat, Slack, Skype and Web)</strong> 
                    using four to five steps in markets with low internet penetration rate and a high basic 
                    mobile phone adoption rate. Using a unique code; products in Newspapers, Static billboards, 
                    Television adverts, Handbills etc. are easily converted into sales by initiating an order 
                    via SMS or other text-based channels; thus, giving a new meaning to offline business 
                    transactions and extending sales beyond the current brick and mortar system. Payment is completed in 
                    real-time via a variety of options including <strong>USSD, Bank Deposit, Pay at ATM, Online Payment</strong> etc.
                </p>
            </div>
            <div class="w-col w-col-4 support-side-nav">
                <h3 class="side-nav-link h3">About Us</h3>
                <a href="<?=site_url('how-it-works') ?>" class="side-nav-link">How it works</a>
                <a href="<?=site_url('channels') ; ?>" class="side-nav-link">Our Channels</a>
                <a href="<?=site_url('pay/ng') ; ?>" class="side-nav-link">Pay with Order ID</a>
                <a href="<?=site_url('contact-us') ?>" class="side-nav-link">Contact Us</a>
            </div>
        </div>
    </div>
</div>