<style>
.field-label-text{
  font-color:#333 !important;
}
</style>
<div class="w-section info--section"></div>
<div class="w-section s-info-holder">
  <div class="w-container">
    <div class="w-row">


      <?=form_open('contact-us') ?>

      <div class="w-col w-col-8">
        <h1 class="support-heading-style">Contact Us</h1>
        
        <?php if(isset($message)): ?>
          <label style="float:left; text-align:left; color:green"><?=isset($message) ? $message : '' ?></label>
          <?php else: ?>

            <div class="w-form">
              <label for="name" class="p-text" style="font-weight: bold;">Name</label>
              <input type="text" name="name" class="w-input textfield" required style="width:70%" placeholder="Full Name">
            </div>

            <div class="w-form">
              <label for="name" class="p-text" style="font-weight: bold;">Email</label>
              <input type="email" name="email" class="w-input textfield" required style="width:70%" placeholder="Valid Email address">
            </div>

            <div class="w-form">
              <label for="name" class="p-text" style="font-weight: bold;">Message</label>
              <textarea name="message" class="w-input textfield" required style="width:70%" rows="6"></textarea>
            </div>

            <div class="third-input">
              <input type="hidden" name="recaptcha" id="recaptcha">
              <button type="submit" class="w-button submit-nx">Continue</button>
             
            </div>

          <?php endif ?>
        </div>
        <?=form_close() ?>

        <div class="w-col w-col-4 support-side-nav">
          <a href="<?=site_url('about-us') ; ?>" class="side-nav-link">About Us</a>
          <a href="<?=site_url('how-it-works') ; ?>" class="side-nav-link">How it works</a>
          <a href="<?=site_url('channels') ; ?>" class="side-nav-link">Our Channels</a>
          <a href="<?=site_url('pay/ng') ; ?>" class="side-nav-link">Pay with Order ID</a>
          <h3 class="side-nav-link h3">Contact Us</h3>
        </div>
      </div>
    </div>
  </div>

  <script src="https://www.google.com/recaptcha/api.js?render=6LehBJIUAAAAAGw5cFZYmSJqH0An1lR8G4zbw18F"></script>
  <script>

    grecaptcha.ready(function() {

      grecaptcha.execute('6LehBJIUAAAAAGw5cFZYmSJqH0An1lR8G4zbw18F', {action: 'contact'}).then(function(token) {

        $('#recaptcha').val(token);

     });

    });
  </script>