<div class="w-section info--section"></div>
<div class="w-section s-info-holder">
    <div class="w-container">
        <div class="w-row">
            <div class="w-col w-col-8">
                <h1 class="support-heading-style">How it works</h1>
                <p class="p-text">
                    Store owners create a unique code for each product on our platform and share it through any means they want, be it Television, Online, BBM Status, Bill board, Handbills etc.
                </p>
                <p class="p-text">
                    Users send the instruction to buy with the product code to <strong>Ogaranya</strong> through our channels (SMS, Twitter, Telegram, Facebook), we process the order and engage the user to the end of the transaction.
                </p>
                <p class="p-text">Happy customer, happier seller!</p>
                <p class="p-text"><img src="<?=base_url() ; ?>/assets/auth/images/how-it-works.jpg"></p>
            </div>
            <div class="w-col w-col-4 support-side-nav">
                <a href="<?=site_url('about-us') ; ?>" class="side-nav-link">About Us</a>
                <h3 class="side-nav-link h3">How it works</h3>
                <a href="<?=site_url('channels') ; ?>" class="side-nav-link">Our Channels</a>
                <a href="<?=site_url('pay/ng') ; ?>" class="side-nav-link">Pay with Order ID</a>
                <a href="<?=site_url('contact-us') ; ?>" class="side-nav-link">Contact Us</a>
            </div>
        </div>
    </div>
</div>
