<div class="w-section info--section"></div>
<div class="w-section s-info-holder">
    <div class="w-container">

        <div class="row">
            <div class="column" style="height:350px;padding:20px;background-color:#88c0d8;text-align: center;">
                <h1>SMS</h1>
                <img src="<?=base_url() ?>assets/auth/images/sms.png"><br /><br />
                Send text message to <a href="https://wa.me/2348107600076">08107600076</a>
            </div>

            <div class="column" style="height:350px;padding:20px;background-color:#a2c8d8;text-align: center;">
                <h1>WHATSAPP</h1>
                <img src="<?=base_url() ?>assets/auth/images/whatsapp.png"><br /><br />
                Send text message to <a href="https://wa.me/2348107600076">08107600076</a>
            </div>
            <div class="column" style="height:350px;padding:20px;background-color:#b5cfda;text-align: center;">
                <h1>FACEBOOK</h1>
                <a href="http://m.me/ogamarket" target="_blank"> <img src="<?=base_url() ?>assets/auth/images/facebook.png"></a><br /><br />
                Send your request through our <a href="http://m.me/ogamarket" target="_blank">chat</a>. Start a <a href="http://m.me/ogamarket" target="_blank">conversation</a> with us..
            </div>
            <div class="column" style="height:350px;padding:20px;background-color:#c9d2d6;text-align: center;">
                <h1>TWITTER</h1>
                <a href="https://twitter.com/ogamarket" target="_blank"><img src="<?=base_url() ?>assets/auth/images/twitter.png"></a><br /><br />
                Send us a Direct Message at <strong><a href="https://twitter.com/ogamarket" target="_blank">@ogamarket</a></strong>,
            </div>

        </div>

        <div class="row">
            <div class="column" style="height:350px;padding:20px;background-color:#88c0d8;text-align: center;">

                <h1>SLACK</h1>
                <a href="https://slack.com/oauth/authorize?client_id=14122120950.14116397412&scope=bot,chat:write:bot,chat:write:user,im:history" target="_blank"><img src="<?=base_url() ?>assets/auth/images/slack.png"></a><br /><br />
                All you need to do is add <strong><a href="https://slack.com/oauth/authorize?client_id=14122120950.14116397412&scope=bot,chat:write:bot,chat:write:user,im:history" target="_blank">Ogaranya</a></strong> to your workspace Slack. It will take care of the rest.
            </div>
            <div class="column" style="height:350px;padding:20px;background-color:#a2c8d8;text-align: center;">
                <h1>TELEGRAM</h1>
                <a href="https://t.me/OgaranyaBot" target="_blank"><img src="<?=base_url() ?>assets/auth/images/telegram.png"></a><br /><br />
                    Search for <strong><a href="https://t.me/OgaranyaBot" target="_blank">@OgaranyaBot</a></strong> in your contact list. Add it and have a new commerce experience.
            </div>
            <div class="column" style="height:350px;padding:20px;background-color:#b5cfda;text-align: center;">
                <h1>WECHAT</h1>
                <img src="<?=base_url() ?>assets/auth/images/wechat.png"><br /><br />
                Our WeChat ID is <strong>ogamarket</strong>. Add us to your contact and send us your request.
            </div>

            <div class="column" style="height:350px;padding:20px;background-color:#c9d2d6;text-align: center;">
                <h1>SKYPE</h1>
                <a href="https://join.skype.com/bot/1852d66d-02a8-43e3-91df-3e6068589a70" target="_blank"><img src="<?=base_url() ?>assets/auth/images/skype.png"></a><br /><br />
                Add <strong><a href="https://join.skype.com/bot/1852d66d-02a8-43e3-91df-3e6068589a70" target="_blank">Ogaranya</a></strong> to your contact and let the conversation begin.
            </div>
        </div>
    </div>
</div>