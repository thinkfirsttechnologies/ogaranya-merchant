 <div class="w-section hero-section">
     <div class="w-container main-wrapper">
         <h1><span class="producre-code-heading">Increase sales by 200%.<br>Make every advert count.</span></h1>
         <form action="<?= site_url('authentication/code') ?>" method="post">
             <div class="w-clearfix form-holder">
                 <div class="first-holder" style="width:100% !important; text-align:center">
                     <div class="w-form">
                         <label for="name" class="field-label-text" style="text-align:center;">Invitation Code</label>
                         <input id="code" type="number" name="code" class="w-input textfield" maxlength="8" placeholder="Invitation Code" required style="width:100%">
                         <p class="click-sign-up" style="width:100%;">
                             TEXT <strong>Merchant</strong> to <strong>08107600076</strong> to get an invitation code.
                         </p>
                         <div class="form-fail" id="errors" text-align:center;">
                             <?= $this->session->userdata('error') ?>
                         </div>
                     </div>
                 </div>

                 <div class="third-input">
                     <button type="submit" class="w-button submit-nx">Continue</button>
                     <div>
                         <p class="click-sign-up">
                             or <span class="sign-in"><a href="<?= site_url('authentication') ?>" class="support-section-link">Sign In</a></span>
                         </p>
                     </div>
                 </div>
             </div>
         </form>
     </div>
 </div>

 <div class="w-section main-section">
     <div class="row">
         <div style="height:50px; background-color: #273765; text-align: center;padding-bottom: 70px;">
             <span class="producre-code-heading">You want to see how it works? Try Demo</span>
         </div>
     </div>
     <div class="row">
         <div class="column" style="height:350px;padding:20px;background-color:#c9d2d6;text-align: center;">
             <h1>Buy</h1>
             <img src="<?= base_url() ?>assets/auth/images/buy.png"><br /><br />

             Text <strong>"Buy 101010"</strong> to <strong>08107600076 </strong> to buy a demo product.
         </div>
         <div class="column" style="height:350px;padding:20px;background-color:#b5cfda;text-align: center;">
             <h1>Book</h1>
             <img src="<?= base_url() ?>assets/auth/images/ticket.png"><br /><br />
             Text <strong>"Book 110011"</strong> to <strong> 08107600076 </strong> to book for a demo event.
         </div>
         <div class="column" style="height:350px;padding:20px;background-color:#a2c8d8;text-align: center;">
             <h1>Give</h1>
             <img src="<?= base_url() ?>assets/auth/images/give.png"><br /><br />
             Text <strong>"Give 111000"</strong> to <strong>08107600076 </strong> to donate to a demo cause.
         </div>
         <div class="column" style="height:350px;padding:20px;background-color:#88c0d8;text-align: center;">
             <h1>Invoice</h1>
             <br>
             <img src="<?= base_url() ?>assets/auth/images/billing.png"><br />
             Text <strong> "Invoice 0908100076 200" </strong> to <strong>08107600076 </strong> to create invoice for your customers.
         </div>
     </div>
 </div>

 <div class="w-section main-section">

     <div class="row">
         <div class="column" style="height:350px;padding:20px;background-color:#88c0d8;text-align: center;">
             <h1>Subscription</h1>
             <img src="<?= base_url() ?>assets/auth/images/subscription.png"><br /><br />
             We enable your customers to be able to subscribe for our services without going through hazzle. All they need do is send us a command, we got you covered.
         </div>
         <div class="column" style="height:350px;padding:20px;background-color:#a2c8d8;text-align: center;">
             <h1>Reward</h1>
             <img src="<?= base_url() ?>assets/auth/images/reward.png"><br /><br />
             You can reward your potential customers and also have their contacts so as to reach them with different offers later. You don't need a bogus application for this.
         </div>
         <div class="column" style="height:350px;padding:20px;background-color:#b5cfda;text-align: center;">
             <h1>Campaign</h1>
             <img src="<?= base_url() ?>assets/auth/images/campaign.png"><br /><br />
             We make your campaign an easy task.You can leave the task to us and monitor your campaign analytics as it grows.
         </div>
         <div class="column" style="height:350px;padding:20px;background-color:#c9d2d6;text-align: center;">
             <h1>2WM</h1>
             <img src="<?= base_url() ?>assets/auth/images/2way.png"><br /><br />
             Let us handle your two way messaging needs. You use your custom number and channel. We take care of all the setup whle you watch your business grow.
         </div>
     </div>
 </div>

 <div class="w-section distribution-channel-layout">
     <div class="w-container">
         <div class="mac-air-holder"><img src="<?= base_url() ?>assets/auth/images/ussd-payment.jpg">
         </div>
         <div class="channel-holder">
             <h1 class="producre-code-heading channel">Our Sales Channels</h1>
             <div class="producre-code-heading produceuct-code-ssub-headinge channel">Your customers can buy from our different sales channels.&nbsp;</div>
             <div>
                 <a href="#" class="w-inline-block channel-link">
                     <img src="<?= base_url() ?>assets/auth/images/sms-01.svg" class="individual-channel">
                     <img src="<?= base_url() ?>assets/auth/images/clipart.png" class="individual-channel">
                     <img src="<?= base_url() ?>assets/auth/images/twitter.svg" class="individual-channel">
                     <img src="<?= base_url() ?>assets/auth/images/tel.svg" class="individual-channel">
                     <img src="<?= base_url() ?>assets/auth/images/facebook-icon.png" class="individual-channel">
                 </a>
             </div>
             <br />
             <a href="<?= site_url('channels') ?>" class="w-button watch-now-link">See More Channels</a>
         </div>
     </div>
 </div>

 <div class="w-section-customers">
     <div class="customer-code-heading">
         <p>Our customers</p>
     </div>
     <div class="w-container">

         <div class="w-row">
             <div class="w-col w-col-4">
                 <img src="<?= base_url() ?>assets/auth/images/lafiyah.png" class="">

             </div>
             <div class="w-col w-col-4">
                 <img src="<?= base_url() ?>assets/auth/images/neolife.png" class="" >

             </div>
             <div class="w-col w-col-4">
                 <img src="<?= base_url() ?>assets/auth/images/PAY-SABIL.png" class="">

             </div>
         </div>

     </div>
 </div>