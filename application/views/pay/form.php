 <h1 class="all-h1-heading create-account">Online Payment</h1>

 <div class="create-account-holder">

  <form name="email-form" method="post" action="<?=site_url('pay/' . $country) ?>">

    <label for="name-2" class="field-label-create">Order ID</label>
    <?php $style = form_error('order_id') == '' ? '' : "style='border-color:red'" ?>
    <input id="name-2" type="number" name="order_id" class="w-input text-field-form" required <?=$style ?> value="<?=set_value('order_id') ?>">
    <div class="form-fail" style="width:100%; text-align:center; margin-bottom:10px;">
      <?=form_error('order_id') ?> <?=s('error') ?>
    </div>
    <input type="submit" value="Confirm Order" class="w-button sign-up">

  </form>

</div>