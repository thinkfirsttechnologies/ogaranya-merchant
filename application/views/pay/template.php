<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <title>Ogaranya - Pay</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="generator" content="Webflow">
  <link rel="stylesheet" type="text/css" href="<?=base_url('assets/auth/css/custom.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?=base_url('assets/auth/css/normalize.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?=base_url('assets/auth/css/webflow.css"') ?>>
  <link rel="stylesheet" type="text/css" href="<?=base_url('assets/auth/css/ogaranya.webflow.css') ?>">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <script>
    WebFont.load({
      google: {
        families: ["Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic"]
      }
    });
  </script>
  <script type="text/javascript" src="<?=base_url('assets/auth/js/modernizr.js') ?>"></script>
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <link rel="shortcut icon" type="image/x-icon" href="<?=base_url('assets/logo.png') ?>">
  <link rel="apple-touch-icon" href="<?=base_url('assets/auth/logo.png') ?>">

  <style type="text/css">
  input[type=number]::-webkit-inner-spin-button,  input[type=number]::-webkit-outer-spin-button {

    -webkit-appearance: none; 
    margin: 0; 
  }

  input[type='number'] {
    
    -moz-appearance:textfield;
  }

  .support-section-link{

    color: #fff !important;
    text-decoration : none;
  }
  
</style>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-90869513-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-90869513-2');
</script>

</head>
<body>

  <div class="w-section create-wrapper payment">
    <div class="w-container container-x">
      <div class="create-account--info">

        <a href="<?=site_url() ?>" class="w-inline-block logo">
          <img width="250" src="<?=base_url('assets/auth/images/logo-01.svg') ?>" class="logo">
        </a>

        <?php if(isset($page)) $this->load->view($page) ?>

      </div>
    </div>
  </div>

  <div class="w-section footer">
    <div class="w-container">
      <div class="support-section">
        <a href="<?=site_url('how-it-works') ?>" class="support-section-link">HOW IT WORKS</a> &bull; <span class="f-nav-link"></span>
          <a href="<?=site_url('about-us') ?>" class="support-section-link">ABOUT US</a>  &bull; <span class="f-nav-link"></span>
          <a href="<?=site_url('channels') ?>" class="support-section-link">OUR CHANNELS</a> &bull; <span class="f-nav-link"></span>
          <a href="<?=site_url('pay') ?>" class="support-section-link">PAY WITH ORDER ID</a> &bull; <span class="f-nav-link"></span>
          <a href="<?=site_url('contact-us') ?>" class="support-section-link">CONTACT US</a>
      </div>
      <div class="sub-footer">
        <a href="#" class="sub-footer-nav">Careers&nbsp;</a>
        <a href="#" class="sub-footer-nav">Terms & Condition</a>
        <a class="sub-footer-nav">Privacy Policy</a>
      </div>
      <div class="copyright-sec">Copyright &copy; <?=date('Y') ; ?> Ogaranya. All rights reserved.</div>
    </div>
  </div>
  <!--[if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif]-->
</body>
</html>