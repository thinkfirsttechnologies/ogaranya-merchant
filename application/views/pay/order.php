 <h1 class="all-h1-heading create-account">Order ID: <?=$order_id ?></h1>

 <div class="create-account-holder">

  <form name="email-form" method="post" action="<?=site_url('pay') ?>">

    <table class="table table-bordered table-striped table-responsive">
      <thead>
        <tr>
          <th>Code</th>
          <th>Product</th>
          <th>Quantity</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($order->cart as $row): ?>
          <tr>
            <td><?=$row->code ?></td>
            <td><?=$row->name ?></td>
            <td><?=$row->qty ?></td>
          </tr>
        <?php endforeach ?>
      </tbody>
    </table>

    <button type="button" class="w-button sign-up" onclick="pay()">
      Pay <?=$order->currency.number_format($order->amount, 2) ?>
    </button>
    
  </form>

<!-- this is the live link right? -->
  <script src="https://kongapay-pg.kongapay.com/js/v1/production/pg.js"></script>

  <script>

    function pay() {

      KPG.setup({

        'amount'          : <?=$amount ?>,
        'description'     : 'Payment for Order <?=$order_id ?> Via Konga Pay',
        'hash'            : '<?=$hash ?>',
        'merchantId'      : '<?=$merchant_id ?>',
        'reference'       : '<?=$trans_ref ?>',
        'callback'        : 'requery',
        'phone'           : '<?=$msisdn ?>',
        'selectedChannelIdentifier' : 'chn_002_in3sw',
        'email'           : 'info@ogaranya.com',
        'enableFrame'     : true

      });
    }

    function requery(error, data){

      window.location = '<?=site_url('payment-response/'.$trans_ref) ?>';

    }
  </script>


</div>
