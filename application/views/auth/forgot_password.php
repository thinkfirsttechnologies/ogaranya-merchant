
<div class="" style="background-repeat: repeat !important;">
  <div class="w-container container-x">
    <div class="create-account--info">
      <a href="<?=site_url() ; ?>" class="w-inline-block logo">
        <img width="208" src="<?=base_url() ?>assets/auth/images/logo-01.svg" class="logo">
      </a>
      <h1 class="all-h1-heading create-account">Forgot Password</h1>
      <div class="create-account-holder">
        <div class="w-form">

          <?=form_open(site_url('forgot-password')) ?>

          <?php if(s(MESSAGE) != ''): ?>
            <div class="form-fail" style="text-align:center"><?=s(MESSAGE) ?></div>
          <?php endif ?>

          <?php if(s('success') != ''): ?>
            <div style="color:green; text-align:center"><?=s('success') ?></div>
          <?php endif ?>

          <label for="email" class="field-label-create">Email Address</label>
          <input class="w-input text-field-form" type="text" name="email" required placeholder="Your Email">

          <input type="submit" value="Send Reset Password Email" data-wait="Please wait..." class="w-button sign-up">

          <p class="click-sign-up">
           <a href="<?=site_url('authentication') ?>" style="color:#222; text-decoration: none">Back to Login</a>
         </p>
         <?=form_close() ?>

       </div>
     </div>
   </div>
 </div>
</div>

</body>
</html>