
<div class="" style="background-repeat: repeat !important;">
  <div class="w-container container-x">
    <div class="create-account--info">
      <a href="<?=site_url() ; ?>" class="w-inline-block logo">
        <img width="208" src="<?=base_url() ?>assets/auth/images/logo-01.svg" class="logo">
      </a>
      <h1 class="all-h1-heading create-account">Merchant Login</h1>
      <div class="create-account-holder">
        <div class="w-form">

          <?=form_open(site_url('authentication')) ?>

          <?php if(s(MESSAGE) != ''): ?><div class="form-fail"><?=s(MESSAGE) ?></div><?php endif ?>
          <?php if(s('success') != ''): ?><div style="color:green; text-align:center">
            <?=s('success') ?></div>
          <?php endif ?>

          <label for="email" class="field-label-create">Email Address</label>
          <input class="w-input text-field-form" type="text" name="email" required placeholder="Your Email" value="<?=set_value('merchant_email') ?>">
          <div class="form-fail"><?=form_error('merchant_email') ; ?></div>

          <label for="email-2" class="field-label-create">Password</label>
          <input class="w-input text-field-form" type="password" name="password" required placeholder="Password">
          <div class="form-fail"><?=form_error('password') ; ?></div>

          <input type="submit" value="Login" data-wait="Please wait..." class="w-button sign-up">

          <p class="click-sign-up">
           <a href="<?=site_url('forgot-password') ?>" style="color:red; text-decoration: none">Forgot your Password?</a>
         </p>
         <?=form_close() ?>

       </div>
     </div>
   </div>
 </div>
</div>

</body>
</html>