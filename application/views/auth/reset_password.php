
<div class="" style="background-repeat: repeat !important;">
  <div class="w-container container-x">
    <div class="create-account--info">
      <a href="<?=site_url() ; ?>" class="w-inline-block logo">
        <img width="208" src="<?=base_url() ?>assets/auth/images/logo-01.svg" class="logo">
      </a>
      <h1 class="all-h1-heading create-account">Reset Password</h1>
      <div class="create-account-holder">
        <div class="w-form">

          <?=form_open(site_url('p/'.$code)) ?>

          <?php if(s(MESSAGE) != ''): ?>
            <div class="form-fail" style="text-align:center"><?=s(MESSAGE) ?></div>
          <?php endif ?>

          <?php if(s('success') != ''): ?>
            <div style="color:green; text-align:center"><?=s('success') ?></div>
          <?php endif ?>

          <?php if($hide_form == 0): ?>

            <label for="email-2" class="field-label-create">New Password</label>
            <input class="w-input text-field-form" type="password" name="password" required placeholder="Password">
            <div class="form-fail"><?=form_error('password') ; ?></div>

            <label for="email-2" class="field-label-create">Confirm Password</label>
            <input class="w-input text-field-form" type="password" name="cpassword" required placeholder="Confirm Password">
            <div class="form-fail"><?=form_error('cpassword') ; ?></div>

            <input type="hidden" name="code" value="<?=$code ?>">

            <input type="submit" value="Reset" data-wait="Please wait..." class="w-button sign-up">

          <?php endif ?>
          <p class="click-sign-up">
           <a href="<?=site_url('authentication') ?>" style="color:#222; text-decoration: none">Back to Login</a>
         </p>

         <?=form_close() ?>

       </div>
     </div>
   </div>
 </div>
</div>

</body>
</html>