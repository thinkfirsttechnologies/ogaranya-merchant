<style>
#count-down{
  width:100%;
  text-align:center;
  color:red;
  margin:0;
}
</style>
<div class="w-section create-wrapper">
  <div class="w-container container-x">
    <div class="create-account--info">

      <a href="<?=site_url() ; ?>" class="w-inline-block logo">
        <img width="208" src="<?=base_url() ?>assets/auth/images/logo-01.svg" class="logo">
      </a>

      <h1 class="all-h1-heading create-account">Merchant Registration</h1>

      <div class="create-account-holder">
        <div class="w-form">
          <?=form_open(site_url('registration/register/'.$code->hash)) ?>
          <div id="count-down"></div>

          <?php if($this->session->userdata('error')): ?>
            <div class="form-fail" style="text-align:center;"><?=$this->session->userdata('error') ?></div>
          <?php endif ?>

          <input class="w-input text-field-form" type="text" value="<?=$code->msisdn ?>" disabled style="border: 1px solid green">

          <label for="name" class="field-label-create">Store Name</label>
          <input class="w-input text-field-form" type="text" name="merchant_name" required placeholder="Store Name" value="<?=set_value('merchant_name') ?>">
          <div class="form-fail"><?=form_error('merchant_name') ; ?></div>

          <label for="name" class="field-label-create">Contact Person Full Name</label>
          <input class="w-input text-field-form" type="text" name="merchant_contact_person" required placeholder="Contact Person Full Name" value="<?=set_value('merchant_contact_person') ?>">
          <div class="form-fail"><?=form_error('merchant_contact_person') ; ?></div>

          <label for="email" class="field-label-create">Email Address</label>
          <input class="w-input text-field-form" type="text" name="merchant_email" required placeholder="Your Email" value="<?=set_value('merchant_email') ?>">
          <div class="form-fail"><?=form_error('merchant_email') ; ?></div>

          <label for="email-2" class="field-label-create">Password</label>
          <input class="w-input text-field-form" type="password" name="password" required placeholder="Password">
          <div class="form-fail"><?=form_error('password') ; ?> </div>

          <label for="email-5" class="field-label-create">Re-Enter Password</label>
          <input class="w-input text-field-form" type="password" name="cpassword" required placeholder="Retype Password">
          <div class="form-fail"><?=form_error('cpassword') ; ?></div>

          <input type="hidden" name="merchant_phone" value="<?=$code->msisdn ?>">
        
          <label for="email-3" class="field-label-create">Store Location</label>
          <textarea class="w-input text-field-form" cols="3" name="merchant_address" required placeholder="Store Location"><?=set_value('merchant_address') ?></textarea>
          <div class="form-fail"><?=form_error('merchant_address') ; ?></div>

          <?=$widget.' '.$script ?>
        </div>


        <input type="submit" value="Sign Up" data-wait="Please wait..." class="w-button sign-up">
        <p class="click-sign-up create">By clicking Sign up, I agree to the Terms of Service and
          <br> Privacy Policy. or 
          <span class="sign-in"><a href="<?=site_url('authentication') ?>">Sign In</a></span>
        </p>

        <?=form_close() ?>

      </div>
    </div>
  </div>
</div>
</div>

</body>
</html>