<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8">
  <title><?=isset($title) ? $title : APP_NAME ?> - Convenient Commerce via SMS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="generator" content="Webflow">
  <link rel="stylesheet" type="text/css" href="<?=base_url() ?>assets/auth/css/normalize.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url() ?>assets/auth/css/webflow.css">
  <link rel="stylesheet" type="text/css" href="<?=base_url() ?>assets/auth/css/ogaranya.webflow.css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <script>
    WebFont.load({
      google: {
        families: ["Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic"]
      }
    });
  </script>
  <script type="text/javascript" src="<?=base_url() ?>assets/auth/js/modernizr.js"></script>
  <link rel="shortcut icon" type="image/x-icon" href="<?=base_url('assets/logo.png') ?>">
  <link rel="apple-touch-icon" href="<?=base_url('assets/logo.png') ?>">
</head>
<body class="w-section create-wrapper">

<?php if(isset($page)) $this->load->view($page) ?>

  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script type="text/javascript" src="<?=base_url() ?>assets/plugins/countdown/jquery.countdown.min.js"></script>
  <script>
  <?php if(isset($end_date)): ?>
    $(document).ready(function(){
      if($('#count-down').length != 0){
       $('#count-down')
       .countdown("<?=date('Y/m/d H:i:s', strtotime($end_date)) ?>", function(event){
        $(this).text(event.strftime('You have %D Day %H Hrs, %M Mins %S Secs left'));
       });
      }
    })
  <?php endif; ?>
  </script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/auth/js/webflow.js"></script>
  <!--[if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif]-->
</body>
</html>
