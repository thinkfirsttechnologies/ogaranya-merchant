
<div class="w-section create-wrapper confirm-order">
    <div class="w-container container-x">
      <div class="create-account--info">
        <a href="<?=site_url() ; ?>" class="w-inline-block logo"><img width="208" src="<?=base_url() ?>assets/auth/images/logo-01.svg" class="logo">
        </a>
        <h1 class="all-h1-heading create-account">Thanks</h1>
        <div class="confirm-account---id" >
          <div class="w-form confirm-order-holder" style="height:170px !important">
              <div class="c-block">
                Thanks for your interest in becoming one of our merchants. Your account is not currently active, we would review your details and get back to you.
                <br />
                We have also sent you a confirmation email. For more information please call 0808 243 1008<br>
                <a href="<?=site_url() ?>" style="cursor:pointer">Home Page</a>
              </div>

            <div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
