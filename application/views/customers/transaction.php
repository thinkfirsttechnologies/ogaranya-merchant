<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row bg-title">
      <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Orders</h4>
      </div>
      <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
          <li><a href="#">Dashboard</a></li>
          <li><a href="#">Customers</a></li>
          <li class="active">Orders</li>
        </ol>
      </div>
      <!-- /.col-lg-12 -->
    </div>
    <!-- /row -->
    <div class="row">
      <div class="col-sm-12">
        <div class="white-box">
          <h3 class="box-title m-b-0">Orders</h3>

          <div role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active">
              <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Orders by <?=$msisdn ?></a>
              </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="home">
                <div class="table-responsive">
                  <table id="datatable6" class="table table-striped">
                    <thead>
                      <tr>
                        <th>&nbsp;</th>
                        <th>Order Ref</th>
                        <th>Customer MSISDN</th>
                        <th>Order Total</th>
                        <th>Order Status</th>
                        <th>Order Date</th>
                        <th>Payment Date</th>
                        <th>Action</th>
                      </tr>
                    </thead>
<!--                    <tbody>-->
<!--                      --><?php //$count = 1; ?>
<!--                      --><?php //if($orders): ?>
<!--                        --><?php //foreach($orders as $row): ?>
<!--                          <tr>-->
<!--                            <td>--><?//=$count++ ?><!--</td>-->
<!--                            <td>--><?//=$row->order_reference ?><!--</td>-->
<!--                            <td>--><?//=$row->msisdn ?><!--</td>-->
<!--                            <td>--><?//=c($row->total) ?><!--</td>-->
<!--                            <td>--><?//=badge($row->order_status) ?><!--</td>-->
<!--                            <td>--><?//=$row->date_ordered ?><!--</td>-->
<!--                            <td>--><?//=$row->payment_date ?><!--</td>-->
<!--                            <td>-->
<!--                              --><?//=form_open('orders/order/'.md5(time())) ?>
<!--                                <input type="hidden" name="order_id" value="--><?//=$row->order_id ?><!--">-->
<!--                                <input type="submit" class="btn btn-info" value="View Details">-->
<!--                              --><?//=form_close() ?>
<!--                            </td>-->
<!--                          </tr>-->
<!--                        --><?php //endforeach; ?>
<!--                      --><?php //endif; ?>
<!--                    </tbody>-->
                    </table>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container-fluid -->
    <?php $this->load->view('includes/footer') ?>
  </div>
  <!-- /#page-wrapper -->
</div>