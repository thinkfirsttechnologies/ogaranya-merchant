   <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Wallet</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
          <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li class="active">Wallet</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- /row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <h3 class="box-title m-b-0">Wallet</h3>
            
            <div role="tabpanel">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">

                <li role="presentation" class="active">
                  <a href="#tab" aria-controls="tab" role="tab" data-toggle="tab">Request Withdrawal</a>
                </li>
                <li role="presentation">
                  <a href="#activity" aria-controls="tab" role="tab" data-toggle="tab">Recent Wallet Activities</a>
                </li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab">
                  <?php $this->load->view('includes/alerts') ?>
                  <div class="row">
                    <div class="col-md-4">

                      <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                Wallet Balance
                              </a>
                            </h4>
                          </div>
                          <div id="collapse1" class="panel-collapse collapse in">
                            <div class="panel-body">
                              <h1><?=c($this->generalModel->merchantGrandBalance()) ?></h1>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                Total Withdrawals
                              </a>
                            </h4>
                          </div>
                          <div id="collapse2" class="panel-collapse collapse">
                            <div class="panel-body">
                              <h1><?=c($this->generalModel->merchantTotalWithdrawal('out')) ?></h1>
                            </div>
                          </div>
                        </div>
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                Wallet Grand Total
                              </a>
                            </h4>
                          </div>
                          <div id="collapse3" class="panel-collapse collapse">
                            <div class="panel-body">
                              <h1><?=c($this->generalModel->merchantGrandBalance() + $this->generalModel->merchantTotalWithdrawal('out')) ?></h1>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-8">
                      <?=form_open('wallet/request/'.md5(time().time())) ?>

                      <div class="form-group">
                        <label for="">Amount</label>
                        <input type="number" class="form-control" name="amount" placeholder="Enter withdrawal amount" required>
                      </div>

                      <div class="form-group">
                        <label for=""><?=DB::getCell(MERCHANTS_ACCOUNT_DETAILS, ['member_id'=>s('merchant_id')], 'security_question'); ?></label>
                        <input type="text" name="security_answer" class="form-control" required>
                      </div>

                      <button type="submit" class="btn btn-info">Submit</button>
                      <?=form_close() ?>
                    </div>
                  </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="activity">
                  <div class="table-responsive">
                    <table id="datatable45" class="table table-striped">
                      <thead>
                        <tr>
                          <th>&nbsp;</th>
                          <th>Date</th>
                          <th>Description</th>
                          <th>Credit</th>
                          <th>Debit</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $count = 1; ?>
                        <?php if($activities): ?>
                          <?php foreach($activities as $row): ?>
                            <tr>
                              <td><?=$count++ ?></td>
                              <td><?=d($row['date'], false) ?></td>
                              <td><?=$row['description'] ?></td>
                              <td class="<?=$row['class'] ?>"><?=$row['credit'] ?></td>
                              <td class="<?=$row['class'] ?>"><?=$row['debit'] ?></td>
                            </tr>
                          <?php endforeach; ?>
                        <?php endif; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        <!-- /.row -->

      </div>
      <!-- /.container-fluid -->
      <?php $this->load->view('includes/footer') ?>
    </div>
  </div>