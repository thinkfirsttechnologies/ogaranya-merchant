
<!-- Bootstrap Core JavaScript -->
<script src="<?=base_url() ?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?=base_url() ?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?=base_url() ?>assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?=base_url() ?>assets/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?=base_url() ?>assets/js/custom.js"></script>
<script src="<?=base_url() ?>assets/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="//cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- end - This is for export functionality only -->

<!--weather icon -->
<script src="<?=base_url() ?>assets/plugins/bower_components/skycons/skycons.js"></script>
<!--Counter js -->
<script src="<?=base_url() ?>assets/plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?=base_url() ?>assets/plugins/bower_components/counterup/jquery.counterup.min.js"></script>
<!-- Custom Theme JavaScript -->
<!-- Sparkline chart JavaScript -->
<script src="<?=base_url() ?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<!--Style Switcher -->
<script src="<?=base_url() ?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
<?php

if(isset($products )) {
    $products ;

$data = array();

$r = 0;
$visible = '';
foreach ($products['columnDefs'] as $product) {

    $data[] = array(
        "targets" => $r,
        "name"       => $product[0],
        "searchable" => $product[1],
        "orderable" => $product[2],
        "visible"   => $product[3],

    );

    $r++;
}
}

?>
<script>
    $(function(){
        $('#datatable').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "order": [[ 0, "asc" ]]
        });

        $('#datatable2').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

    })
</script>

<?php if(isset($products )) :?>
<script>
    $(function(){

        $('#datatable6').DataTable( {

            dom: 'Blfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "processing": true,
            "serverSide": true,
            "autowidth" : false,
            "lengthMenu": [[10, 25, 50,100, 200,500, 1000, -1], [10, 25, 50,100, 200,500,1000, "All"]],
            "ajax": "<?= $products['url'] ?>",
            "order": [[<?= $products['order'][0] ?>, '<?= $products['order'][1] ?>']],
            "columnDefs": <?= json_encode($data)?>

        });

    })
</script>

<?php endif ?>
<?php if(isset($scripts)) $this->load->view($scripts) ?>