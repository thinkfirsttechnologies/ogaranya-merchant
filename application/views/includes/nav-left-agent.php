<?php 

$total_products     = DB::numRows(PRODUCTS, ['merchant_id'=>s('merchant_id'), 'product_type'=>PRODUCT]);
$total_events       = DB::numRows(PRODUCTS, ['merchant_id'=>s('merchant_id'), 'product_type'=>EVENT]);
$total_donations    = DB::numRows(PRODUCTS, ['merchant_id'=>s('merchant_id'), 'product_type'=>DONATION]);
$total_subscribers  = DB::numRows(CUSTOM_INSERT, ['merchant_id'=>s('merchant_id')]);
$url                = s('merchant_status') == DISABLED ? false : true;
?>

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse slimscrollsidebar" <?=!$url ? 'style="margin-top:20px"' : '' ?>>
   <ul class="nav" id="side-menu">
    <li class="sidebar-search hidden-sm hidden-md hidden-lg">
      
      <div class="input-group custom-search-form">
        <input type="text" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
        </span> </div>
        
      </li>
      
      <li> 
        <a href="javascript:;" class="waves-effect">
          &nbsp;
        </a>
      </li>
      
      <li> 
        <a href="<?=site_url('home') ?>" class="waves-effect active">
          <i class="icon-home"></i> 
          <span class="hide-menu"> 
            <?=l('dashboard') ?> 
          </span>
        </a>
      </li>


      <li> 
        <a href="<?=$url == true ? site_url('agent/transactions') : '#' ?>" class="waves-effect">
          <i class="icon-basket"></i> 
          <span class="hide-menu"> 
            <?=l('transactions') ?> 
          </span>
        </a>
      </li>

      <li> 
        <a href="<?=site_url('authentication/logout') ?>" class="waves-effect">
          <i class="icon-logout"></i> 
          <span class="hide-menu"> 
            Logout
          </span>
        </a>
      </li>


    </ul>
  </div>
</div>
