<?php 

$total_products       = DB::numRows(PRODUCTS, ['merchant_id'=>s('merchant_id'), 'product_type'=>PRODUCT]);
$total_events         = DB::numRows(PRODUCTS, ['merchant_id'=>s('merchant_id'), 'product_type'=>EVENT]);
$total_donations      = DB::numRows(PRODUCTS, ['merchant_id'=>s('merchant_id'), 'product_type'=>DONATION]);

?>

<div class="navbar-default sidebar" role="navigation">
  <div class="sidebar-nav navbar-collapse slimscrollsidebar">
   <ul class="nav" id="side-menu">
    <li class="sidebar-search hidden-sm hidden-md hidden-lg">
      
      <div class="input-group custom-search-form">
        <input type="text" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
        </span> </div>
        
      </li>
      
      <li> 
        <a href="javascript:;" class="waves-effect">
          &nbsp;
        </a>
      </li>
      
      <li> 
        <a href="<?=site_url('admin') ?>" class="waves-effect active">
          <i class="icon-home"></i> 
          <span class="hide-menu"> 
            <?=l('dashboard') ?> 
          </span>
        </a>
      </li>

      <li>
        <a href="#" class="waves-effect">
          <i class="icon-people"></i> 
          <span class="hide-menu">Merchants
            <span class="fa arrow"></span>
          </span>
        </a>
        <ul class="nav nav-second-level">
          <li>
            <a href="<?=site_url('admin/parent_merchants/') ?>">
              Parent Merchants
            </a>
          </li>
          <li>
            <a href="<?=site_url('admin/merchants/') ?>">
              Merchants
            </a>
          </li>
          
        </ul>
      </li>


      <li> 
        <a href="<?=site_url('admin/agents') ?>" class="waves-effect">
          <i class="icon-people" data-icon="v"></i> 
          <span class="hide-menu"> 
            Agents
          </span>
        </a>
      </li>

      <li> 
        <a href="<?=site_url('admin/products') ?>" class="waves-effect">
          <i class="icon-list" data-icon="v"></i> 
          <span class="hide-menu"> 
            All Products
          </span>
        </a>
      </li>

      <li>
        <a href="#" class="waves-effect">
          <i class="icon-wallet"></i> 
          <span class="hide-menu">Withdrawal Requests
            <span class="fa arrow"></span>
          </span>
        </a>
        <ul class="nav nav-second-level">
          <li>
            <a href="<?=site_url('admin/withdrawals/') ?>">
              Pending Withdrawals
            </a>
          </li>
          <li>
            <a href="<?=site_url('admin/withdrawals_paid/') ?>">
              Approved Withdrawals
            </a>
          </li>
          
        </ul>
      </li>

    </ul>
  </div>
</div>
