 <!-- Navigation -->
 <?php $url  = s('merchant_status') == DISABLED ? false : true; ?>
 <div class="navbar-header"> 
  <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
  <div class="top-left-part">
    <a class="logo" href="<?=site_url() ?>"><b><img src="<?=base_url() ?>assets/logo.png" alt="home" /></b><span class="hidden-xs"><img src="<?=base_url() ?>assets/logo-text.png" alt="home" /></span></a>
  </div>
  <ul class="nav navbar-top-links navbar-left hidden-xs">
    <li>
      <a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a>
    </li>
    <li>
      <form role="search" class="app-search hidden-xs">
        <input type="text" placeholder="Search..." class="form-control">
        <a href="#"><i class="fa fa-search"></i></a>
      </form>
    </li>
    <?php if(s('logged_in_admin') != ''): ?>
      <?php $url = s('agent_impersonate') ? 'agent' : 'home' ?>
      <li>
        <a href="<?=site_url($url.'/return_to_admin') ?>">
          Return to admin Account 
        </a>
      </li>
      <li><a href="#"><i class="fa fa-fire"></i> <?=s('impersonate') ?> <i class="fa fa-fire"></i></a></li>
    <?php endif; ?>
  </ul>
  <ul class="nav navbar-top-links navbar-right pull-right">
    <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-envelope"></i>
      <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
    </a>
    <ul class="dropdown-menu mailbox animated bounceInDown">
      <li>
        <div class="drop-title">You have 0 new notification</div>
      </li>
        <!-- <li>
          <div class="message-center"> 
            <a href="#">
              <div class="user-img"> <img src="<?=base_url() ?>assets/logo.png" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
              <div class="mail-contnet">
                <h5>Akinyele Olubodun</h5>
                <span class="mail-desc">New Order via Facebook</span> <span class="time">9:30 AM</span> 
              </div>
            </a>
          </div>
        </li> -->
        <li> <a class="text-center" href="javascript:void(0);"> <strong>See all notifications</strong> <i class="fa fa-angle-right"></i> </a></li>
      </ul>
      <!-- /.dropdown-messages -->
    </li>
    <!-- /.dropdown -->

    <!-- /.dropdown -->
    <li class="dropdown"> 
      <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> 
        <img src="<?=base_url() ?>assets/logo.png" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">
          <?=ucwords(s('merchant_name')) ?></b> 
        </a>
        <ul class="dropdown-menu dropdown-user animated flipInY">
          <li><a href="#"><i class="ti-user"></i> Change Password</a></li>
          <?php if(s('merchant_id') != ''): ?>
            <li><a href="<?=$url == true ? site_url('wallet')  : '#' ?>"><i class="ti-wallet"></i> Wallet</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="<?=$url == true ? site_url('account') : '#' ?>"><i class="ti-settings"></i> Account Setting</a></li>
          <?php endif; ?>
          <li role="separator" class="divider"></li>
          <li><a href="<?=site_url('authentication/logout') ?>"><i class="fa fa-power-off"></i> Logout</a></li>
        </ul>
        <!-- /.dropdown-user -->
      </li>

      <!-- /.dropdown -->
    </ul>
  </div>
  <!-- /.navbar-header -->
  <!-- /.navbar-top-links -->
  <!-- /.navbar-static-side -->
</nav>