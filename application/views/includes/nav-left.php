<?php

$total_products = DB::numRows(PRODUCTS, ['merchant_id' => s('merchant_id'), 'product_type' => PRODUCT]);
$total_events = DB::numRows(PRODUCTS, ['merchant_id' => s('merchant_id'), 'product_type' => EVENT]);
$total_donations = DB::numRows(PRODUCTS, ['merchant_id' => s('merchant_id'), 'product_type' => DONATION]);
$total_subscribers = DB::numRows(CUSTOM_INSERT, ['merchant_id' => s('merchant_id')]);
$url = s('merchant_status') == DISABLED ? false : true;

?>
<!-- Left navbar-header -->
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar" <?= !$url ? 'style="margin-top:20px"' : '' ?>>
        <ul class="nav" id="side-menu">
            <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                <!-- input-group -->
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
          <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
        </span></div>
                <!-- /input-group -->
            </li>

            <li>
                <a href="javascript:" class="waves-effect">
                    &nbsp;
                </a>
            </li>

            <li>
                <a href="<?php echo site_url('home') ?>" class="waves-effect active">
                    <i class="icon-home"></i>
                    <span class="hide-menu">
            <?php echo l('dashboard') ?>
          </span>
                </a>
            </li>
            <li>
                <a href="#" class="waves-effect">
                    <i class="icon-grid"></i>
                    <span class="hide-menu"><?= l('services') ?>
            <span class="fa arrow"></span>
          </span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php echo $url == true ? site_url('products/index/product') : '#' ?>">
                            <?php echo l('products') ?>
                            <span class="label label-rouded label-info pull-right"><?= $total_products ?></span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo $url == true ? site_url('products/index/event') : '#' ?>">
                            <?php echo l('events') ?>
                            <span class="label label-rouded label-info pull-right"><?= $total_events ?></span>
                        </a>
                    </li>
                    <!-- <li>
            <a href="<?php echo $url == true ? site_url('products/index/donation') : '#' ?>">
              <?php echo l('donations') ?>
              <span class="label label-rouded label-info pull-right"><?= $total_events ?></span>
            </a>
          </li> -->

                </ul>
            </li>

            <li>
                <a href="<?php echo $url == true ? site_url('orders') : '#' ?>" class="waves-effect">
                    <i class="icon-basket"></i>
                    <span class="hide-menu">
            <?php echo l('all Transactions') ?>
          </span>
                </a>
            </li>

            <?php
            $res = $this->db->where(['is_parent' => 1, 'id' => s('merchant_id')])->get(MERCHANT)->num_rows();
            if ($res > 0) { ?>
                <li>
                    <a href="<?php echo $url == true ? site_url('my_merchants') : '#' ?>" class="waves-effect">
                        <i class="icon-people"></i>
                        <span class="hide-menu">
            <?php echo l('My Merchants') ?>
          </span>
                    </a>
                </li>
            <?php } ?>

            <li>
                <a href="<?php echo $url == true ? site_url('wallet') : '#' ?>" class="waves-effect">
                    <i class="icon-wallet"></i>
                    <span class="hide-menu">
            <?php echo l('wallet') ?>
          </span>
                </a>
            </li>

            <li>
                <a href="<?php echo $url == true ? site_url('customers') : '#' ?>" class="waves-effect">
                    <i class="icon-people"></i>
                    <span class="hide-menu">
            <?php echo l('customers') ?>
          </span>
                </a>
            </li>

            <li>
                <a href="<?php echo $url == true ? site_url('products/subscribers') : '#' ?>" class="waves-effect">
                    <i class="icon-user-following"></i>
                    <span class="hide-menu">
            <?php echo l('subscribers') ?>
            <span class="label label-rouded label-info pull-right"><?php echo $total_subscribers ?></span>
          </span>
                </a>
            </li>

            <li>
                <a href="#" class="waves-effect">
                    <i class="icon-user-following"></i>
                    <span class="hide-menu"> <?= l('developers API') ?>
            <span class="fa arrow"></span>
          </span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php echo $url == true ? site_url('developer/test') : '#' ?>">
                            <?php echo l('test') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo $url == true ? site_url('developer/live') : '#' ?>">
                            <?php echo l('live') ?>
                        </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="<?php echo $url == true ? site_url('account') : '#' ?>" class="waves-effect">
                    <i class="icon-settings" data-icon="v"></i>
                    <span class="hide-menu">
            <?= l('account_settings') ?>
          </span>
                </a>
            </li>

            <!-- <li>
        <a href="<?php echo $url == true ? site_url('reports') : '#' ?>" class="waves-effect">
          <i class="fa fa-hourglass-start" data-icon="v"></i> 
          <span class="hide-menu"> 
            <?php echo l('reports') ?>
          </span>
        </a>
      </li> -->

        </ul>
    </div>
</div>
