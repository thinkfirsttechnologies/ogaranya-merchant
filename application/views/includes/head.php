<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" sizes="16x16" href="<?=base_url() ?>assets/plugins/images/logo.png">
	<title><?=isset($title) ? $title : APP_NAME ?></title>
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Ubuntu:regular,bold&subset=Latin">
	<link href="<?=base_url() ?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?=base_url() ?>assets/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
	<link href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
	<link href="<?=base_url() ?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
	<link href="<?=base_url() ?>assets/plugins//bower_components/morrisjs/morris.css" rel="stylesheet">
	<link href="<?=base_url() ?>assets/css/animate.css" rel="stylesheet">
	<link href="<?=base_url() ?>assets/css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">
	<link href="<?=base_url() ?>assets/css/colors/blue.css" id="theme"  rel="stylesheet">
    <link href="<?=base_url() ?>assets/css/custom.css"  rel="stylesheet">
	<link href="<?=base_url() ?>assets/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
	<link href="<?=base_url() ?>assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="shortcut icon" type="image/x-icon" href="<?=base_url('assets/logo.png') ?>">
	<link rel="apple-touch-icon" href="<?=base_url('assets/logo.png') ?>">
	<style type="text/css">
	
	input[type=number]::-webkit-inner-spin-button,  input[type=number]::-webkit-outer-spin-button {

		-webkit-appearance: none; 
		margin: 0; 
	}

	input[type='number'] {

		-moz-appearance:textfield;
	}
	
	.top-disabled{

		background: red;
		color: #fff;
		text-align:center;
		padding-bottom: 2px;
		height: 20px;
	}
</style>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<?php if(isset($styles)) $this->load->view('styles') ?>
<!-- jQuery -->
<script src="<?=base_url() ?>assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript">
	//General js variables
	var site_url = "<?=site_url() ?>"
	var base_url = "<?=base_url() ?>"
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-90869513-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-90869513-2');
</script> -->


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-163806396-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date()); gtag('config', 'UA-163806396-1');
</script>

</head>
