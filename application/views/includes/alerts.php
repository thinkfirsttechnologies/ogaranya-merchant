<?php if(s(MESSAGE) != ''): ?>
	<div class="alert alert-info">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>

        <?=s(MESSAGE) ?>
	</div>
<?php endif; ?>

<?php if(s(SUCCESS) != ''): ?>
	<div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>

        <?=s(SUCCESS) ?>
	</div>
<?php endif; ?>

<?php if(s(ERROR) != ''): ?>
	<div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>

        <?=s(ERROR) ?>
	</div>
<?php endif; ?>

<?php if(validation_errors() != ''): ?>
	<div class="alert alert-danger">
		<?=validation_errors() ?>
	</div>
<?php endif; ?>

<?php if(s('success_count') != ''): ?>
	<div class="alert alert-danger">
		<?=s('success_count') ?>
	</div>
<?php endif; ?>

<?php if(s('link') != ''): ?>
	<div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>

        <?=s('link') ?>
	</div>
<?php endif; ?>

<?php if(s('impersonate') != ''): ?>
	<div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>

        <?=s('impersonate') ?>
	</div>
<?php endif; ?>