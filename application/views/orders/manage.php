<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title"><?= $nam= isset($name) ? $name." 's ": ''?>Transactions</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Transactions</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">

                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Transactions</a>
                            </li>
                            <!-- <li role="presentation">
                              <a href="#tab" aria-controls="tab" role="tab" data-toggle="tab">Events</a>
                            </li> -->
                        </ul>
           <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <div class="table-responsive">
                                    <table id="datatable6" class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>&nbsp;</th>
                                            <th>Transaction Date</th>
                                            <th>Reference Number</th>
                                            <th>Customer Phone</th>
                                            <th>Total</th>
                                            <th>Status</th>
                                            <th>Payment Date</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>

                                    </table
                                </div>
                            </div>
                        </div>
                        <!--   <div role="tabpanel" class="tab-pane" id="tab">
                            <div class="table-responsive">
                              <table id="datatable2" class="table table-striped">
                                <thead>
                                  <tr>
                                    <th>Reference Number</th>
                                    <th>Customer Phone</th>
                                    <th>Total</th>
                                    <th>Status</th>
                                    <th>Transaction Date</th>
                                    <th>Payment Date</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                </table
                              </div>
                            </div>
                          </div> -->
                    </div>

                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->
    <?php $this->load->view('includes/footer') ?>
</div>
<!-- /#page-wrapper -->

</div>



