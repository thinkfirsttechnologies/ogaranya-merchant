<script src="<?=base_url() ?>assets/plugins/bower_components/raphael/raphael-min.js"></script>
  <script src="<?=base_url() ?>assets/plugins/bower_components/morrisjs/morris.js"></script>
  <script>
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    Morris.Bar({
      element: 'chart',
      data: [{
        m: '2015-01',
        a: 0,
        b: 270
      }, {
        m: '2015-02',
        a: 54,
        b: 256
      }, {
        m: '2015-03',
        a: 243,
        b: 334
      }, {
        m: '2015-04',
        a: 206,
        b: 282
      }, {
        m: '2015-05',
        a: 161,
        b: 58
      }, {
        m: '2015-06',
        a: 187,
        b: 0
      }, {
        m: '2015-07',
        a: 210,
        b: 0
      }, {
        m: '2015-08',
        a: 204,
        b: 0
      }, {
        m: '2015-09',
        a: 224,
        b: 0
      }, {
        m: '2015-10',
        a: 301,
        b: 0
      }, {
        m: '2015-11',
        a: 262,
        b: 0
      }, {
        m: '2015-12',
        a: 199,
        b: 0
      }, ],
      xkey: 'm',
      ykeys: ['a', 'b'],
      labels: ['2014', '2015'],
    xLabelFormat: function (x) { // <-- changed
      var month = months[x.x];
      return month;
    },
  });
</script>