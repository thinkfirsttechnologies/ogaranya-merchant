<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row bg-title">
      <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Reports</h4>
      </div>
      <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
          <li><a href="#">Dashboard</a></li>
          <li class="active">Reports</li>
        </ol>
      </div>
      <!-- /.col-lg-12 -->
    </div>
    <!-- /row -->
    <div class="row">
      <div class="col-sm-12">
        <div class="white-box">
          <h3 class="box-title m-b-0">General Business Reports</h3>
          <div class="row">
            <div class="col-lg-4 col-sm-4">
              <div class="panel panel-info">
                <div class="panel-heading"> Wallet Summary
                  <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a> <a href="#" data-perform="panel-dismiss"><i class="ti-close"></i></a> </div>
                </div>
                <div class="panel-wrapper collapse in" aria-expanded="true">
                  <div class="panel-body">
                    <table class="table table-condensed table-hover">
                      <tr>
                        <td><strong>Wallet Grand Total</strong></td>
                        <td><?=c($this->generalModel->merchantGrandBalance()) ?></td>
                      </tr>
                      <tr>
                        <td><strong>Wallet Balance</strong></td>
                        <td><?=c($this->generalModel->merchantWalletBalance()) ?></td>
                      </tr>
                      <tr>
                        <td><strong>Total Withdrawals</strong></td>
                        <td><?=c($this->generalModel->merchantTotalWithdrawals()) ?></td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-8">
              <div id="chart">

              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
    <!-- /.row -->

  </div>
  <!-- /.container-fluid -->
  <?php $this->load->view('includes/footer') ?>
</div>