<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row bg-title">
      <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?=$title ?></h4>
      </div>
      <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
          <li><a href="#">Dashboard</a></li>
          <li class="active"><?=$title ?></li>
        </ol>
      </div>

    </div>

    <div class="row">
      <div class="col-sm-12">
        <div class="white-box">

          <table id="datatable" class="table table-striped">
            <thead>
              <tr>
                <th>&nbsp;</th>
                <th>Bill</th>
                <th>Order Reference</th>
                <th>Phone</th>
                <th>Amount</th>
                <th>Logs</th>
                <th>Status</th>
                <th>Date</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $count = 1; ?>
              <?php foreach($transactions as $row): ?>

                <?php $order = DB::firstOrNew('orders', ['order_id' => $row->order_id]); ?>
                
                <tr>
                  <td><?=$count++ ?></td>
                  <td><?=DB::firstOrNew('bills', ['id' => $row->bills_id])->bills_name ?></td>
                  <td><?=$order->order_reference ?></td>
                  <td><?=$order->msisdn ?></td>
                  <td><?=c($row->amount) ?></td>
                  <td><?=$row->custom_log ?></td>
                  <td><?=badge($row->status) ?></td>
                  <td><?=d($row->created_at) ?></td>
                  <td>
                    <a href="<?=site_url('agent/transaction/'.$row->id) ?>" class="btn btn-info btn-xs btn-outline">
                      View Details
                    </a>
                  </td>
                </tr>
                
              <?php endforeach; ?>
            </tbody>
          </table>

        </div>
      </div>

    </div>

  </div>

  <?php $this->load->view('includes/footer') ?>
</div>

</div>