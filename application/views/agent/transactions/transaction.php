<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row bg-title">
      <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Transactions</h4>
      </div>
      <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
          <li><a href="#">Dashboard</a></li>
          <li class="active">Transaction Details</li>
        </ol>
      </div>
    </div>
    
    <div class="row">
      <div class="col-sm-12">
        <div class="white-box">
          <h3 class="box-title m-b-0">Customer Transaction Details</h3>

          <div role="tabpanel">

            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active">
                <a href="#home" aria-controls="home" role="tab" data-toggle="tab"><?=$title ?></a>
              </li>
              
            </ul>

            
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="home">
                <div class="table-responsive">
                  <div class="row">
                    <div class="col-md-12">
                      <?=form_open('orders') ?>

                      <div class="row">

                        <div class="form-group col-md-6">
                          <label>Payment Status</label>
                          <input type="text" class="form-control" value="<?=ucwords($transaction->status) ?>" readonly>
                        </div>
                        <div class="form-group col-sm-6">
                          <label><a href="#">Customer Phone</a></label>
                          <input type="text" class="form-control" value="<?=$transaction->msisdn ?>" readonly>
                        </div>
                      </div>
                    

                      <div class="row">
                        <div class="form-group col-sm-6">
                          <label>Transaction Total</label>
                          <input type="text" class="form-control" value="<?=c($transaction->amount) ?>" readonly>
                        </div>

                        <div class="form-group col-sm-6">
                          <label>Order Reference</label>
                          <input type="text" class="form-control" value="<?=$order->order_reference ?>" readonly>
                        </div>
                      </div>


                      <div class="row">
                        <div class="form-group col-md-6">
                          <label>Logs</label>
                          <input type="text" class="form-control" value="<?=$transaction->custom_log ?>" readonly>
                        </div>
                        <div class="form-group col-md-6">
                          <label>Transaction Date</label>
                          <input type="text" class="form-control" value="<?=$transaction->created_at ?>" readonly>
                        </div>
                      </div>

                      
                    </div>

                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>

      </div>


    </div>

    <?php $this->load->view('includes/footer') ?>
  </div>

</div>