   <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title"><?=ucwords($title) ?></h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
          <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active"><?=$title ?></li>
          </ol>
        </div>

      </div>

      <div class="row">
        <div class="col-lg-3 col-sm-3 col-xs-12">
          <div class="white-box analytics-info">
            <h3 class="box-title">Transactions Count</h3>
            <ul class="list-inline two-part">
              <li>
                <div id="sparklinedash"></div>
              </li>
              <li class="text-right"><span class="counter text-success"><?=number_format($count) ?></span></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-sm-3 col-xs-12">
          <div class="white-box analytics-info">
            <h3 class="box-title">Completed Trans. Total</h3>
            <ul class="list-inline two-part">
              <li>
                <div id="sparklinedash2"></div>
              </li>
              <li class="text-right"><span class="counter text-purple"><?=number_format($total) ?></span></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-sm-3 col-xs-12">
          <div class="white-box analytics-info">
            <h3 class="box-title">Completed Count</h3>
            <ul class="list-inline two-part">
              <li>
                <div id="sparklinedash3"></div>
              </li>
              <li class="text-right"><span class="counter text-info"><?=number_format($completed) ?></span></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-sm-3 col-xs-12">
          <div class="white-box analytics-info">
            <h3 class="box-title">Commission Made</h3>
            <ul class="list-inline two-part">
              <li>
                <div id="sparklinedash4"></div>
              </li>
              <li class="text-right"><span class="counter text-danger"><?=number_format($balance) ?></span></li>
            </ul>
          </div>
        </div>
      </div>


      <div class="row">
        <div class="col-md-12">
          <div class="white-box">
            <div class="row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <h3>
                    Recently Completed Transactions 
                    <?php if(count($transactions) > 0): ?>
                      <a href="<?=site_url('agent/transactions') ?>" class="btn btn-xs btn-outline btn-info pull-right">
                        <i class="icon-link"></i> View All
                      </a>
                    <?php endif ?>
                  </h3>
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Bill</th>
                        <th>Customer Phone</th>
                        <th>Amount</th>
                        <th>Order Reference</th>
                        <th>Transaction Date</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php foreach($transactions as $row): ?>
                        
                        <?php $order = DB::firstOrNew('orders', ['order_id' => $row->order_id]); ?>

                        <tr>
                          <td><?=DB::firstOrNew('bills', ['id' => $row->bills_id])->bills_name ?></td>
                          <td><?=$order->msisdn ?></td>
                          <td><?=c($row->amount) ?></td>
                          <td><?=$order->order_reference ?></td>
                          <td><?=d($row->created_at) ?></td>
                        </tr>
                      <?php endforeach; ?>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('includes/footer') ?>
</div>