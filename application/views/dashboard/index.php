   <!-- Page Content -->
   <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h4 class="page-title">Dashboard</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
          <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- .row -->
      <div class="row">
        <div class="col-lg-3 col-sm-3 col-xs-12">
          <div class="white-box analytics-info">
            <h3 class="box-title">Total Transactions</h3>
            <ul class="list-inline two-part">
              <li>
                <div id="sparklinedash"></div>
              </li>
              <li class="text-right"><span class="counter text-success"><?=number_format($total_orders) ?></span></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-sm-3 col-xs-12">
          <div class="white-box analytics-info">
            <h3 class="box-title">Total Transactions (<?=currency() ?>)</h3>
            <ul class="list-inline two-part">
              <li>
                <div id="sparklinedash3"></div>
              </li>
              <li class="text-right"><span class="counter text-info"><?=number_format($total_orders_amount) ?></span></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-sm-3 col-xs-12">
          <div class="white-box analytics-info">
            <h3 class="box-title">Wallet Balance (<?=currency() ?>)</h3>
            <ul class="list-inline two-part">
              <li>
                <div id="sparklinedash2"></div>
              </li>
              <li class="text-right"><span class="counter text-purple"><?=number_format($wallet_balance) ?></span></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-sm-3 col-xs-12">
          <div class="white-box analytics-info">
            <h3 class="box-title">Highest Transaction (<?=currency() ?>)</h3>
            <ul class="list-inline two-part">
              <li>
                <div id="sparklinedash4"></div>
              </li>
              <li class="text-right"><span class="counter text-danger"><?=number_format($highest_order) ?></span></li>
            </ul>
          </div>
        </div>
      </div>
 
      <!-- .row -->
      <div class="row">
        <div class="col-md-12">
          <div class="white-box">
            <div class="row">
              <div class="col-md-5 col-sm-12 col-xs-12">
                <div class="white-box">
                  <h3 class="box-title">Activity Log</h3>
                  <div class="steamline">
                    <?php foreach($user_activity as $row): ?>
                      <div class="sl-item">
                        <div class="sl-left"> 
                          <img class="img-circle" alt="user" src="<?=base_url() ?>assets/logo.png"> 
                        </div>
                        <div class="sl-right">
                          <div>
                            <a href="#">Merchant</a> 
                            <span class="sl-date">
                              <?=date('Y-m-d g:i A', strtotime($row->created_at)) ?>
                            </span>
                          </div>
                          <p><?=$row->activity ?></p>
                        </div>
                      </div>
                    <?php endforeach; ?>
                  </div>
                </div>
              </div>
              <div class="col-md-7">
                <div class="table-responsive">
                  <h3>
                    Recently Completed Transactions 
                    <?php if(count($recent_orders) > 0): ?>
                      <a href="<?=site_url('orders') ?>" class="btn btn-xs btn-outline btn-info pull-right">
                        <i class="icon-link"></i> View All
                      </a>
                    <?php endif ?>
                  </h3>
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Order Reference</th>
                        <th>Customer Phone</th>
                        <th>Transaction Date</th>
                        <th>Amount</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $count = 1; ?>
                      <?php foreach($recent_orders as $row): ?>
                        <tr>
                          <td><?=$row->order_reference ?></td>
                          <td><?=$row->msisdn ?></td>
                          <td><?=d($row->date_ordered) ?></td>
                          <td><?=c($row->total) ?></td>
                        </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--/.row -->


  </div>
  <!-- /.container-fluid -->
  <?php $this->load->view('includes/footer') ?>
</div>