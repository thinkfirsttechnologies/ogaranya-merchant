 <h1 class="all-h1-heading create-account">Slack Authorization Status</h1>

<style type="text/css">
	p{

		text-align: center;
		font-size: 25px;
		line-height: 40px;
	}

	.error{

		color : red;
	}

	.success{

		color : green;
	}
</style>
 <div class="create-account-holder">

 	<?php if(strlen($error) > 1): ?>
 		<p class="error"><?=$error ?></p>
 	<?php endif ?>

 	<?php if(strlen($success) > 1): ?>
 		<p class="success"><?=$success ?></p>
 	<?php endif ?>

 </div>