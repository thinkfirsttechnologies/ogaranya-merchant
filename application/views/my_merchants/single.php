<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Transactions</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active">Transaction Details</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Customer Transaction Details</h3>

                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#home" aria-controls="home" role="tab" data-toggle="tab"><?=$order->name ?></a>
                            </li>

                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <div class="table-responsive">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <?=form_open('orders') ?>

                                            <div class="row">

                                                <div class="form-group col-md-6">
                                                    <label>Payment Status</label>
                                                    <input type="text" class="form-control" value="<?= ucwords(k($order->order_status)) ?>" readonly>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label><a href="#">Customer Phone</a></label>
                                                    <input type="text" class="form-control" value="<?=$order->msisdn ?>" readonly>
                                                </div>
                                            </div>




                                            <div class="form-group">
                                                <label>Customer Name</label>
                                                <input type="text" class="form-control" value="<?=$order->name ?>" readonly>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-sm-6">
                                                    <label>Transaction Total</label>
                                                    <input type="text" class="form-control" value="<?=c($order->total) ?>" readonly>
                                                </div>

                                                <div class="form-group col-sm-6">
                                                    <label>Transaction Reference</label>
                                                    <input type="text" class="form-control" value="<?=$order->order_reference ?>" readonly>
                                                </div>
                                            </div>

                                            <?php if($order->shipping_address != ''): ?>
                                                <div class="form-group">
                                                    <label>Shipping Address</label>
                                                    <textarea class="form-control" readonly><?=$order->shipping_address ?></textarea>
                                                </div>
                                            <?php endif ?>

                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label>Transaction Date</label>
                                                    <input type="text" class="form-control" value="<?=$order->date_ordered ?>" readonly>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Payment Date</label>
                                                        <input type="text" class="form-control" value="<?=ucwords($order->payment_date) ?>" readonly>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- <div class="form-group">
                                               <label>Delivery Status</label>
                                               <input type="text" class="form-control" value="<//?=ucwords($order->delivery_status) ?>" readonly>
                                             </div>-->
                                            <?=form_close() ?>
                                        </div>
                                        <div class="col-md-6">
                                            <?php $customer_order = json_decode($order->products_purchased , TRUE); ?>
                                            <table class="table table-bordered table-striped">
                                                <thead>
                                                <tr><th colspan="4" class="text-center">Order Details</th></tr>
                                                <tr>
                                                    <th>P. Code</th>
                                                    <th>P. Name</th>
                                                    <th>Qty</th>
                                                    <th>Total</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php for($i = 0; $i<count($customer_order); $i++): ?>
                                                    <tr>
                                                        <td><?=$customer_order[$i][0] ?></td>
                                                        <td><?=DB::firstOrNew(PRODUCTS, ['product_code' => $customer_order[$i][0]])->product_name ?></td>
                                                        <td><?=$customer_order[$i][1] ?></td>
                                                        <td><?=c($order->total) ?></td>
                                                    </tr>
                                                <?php endfor; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>


                                    <?php //var_dump($order);  ?>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->
    <?php $this->load->view('includes/footer') ?>
</div>
<!-- /#page-wrapper -->
</div>