<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title"><?= l('subscribers') ?></h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#"><?= l('dashboard') ?></a></li>
                    <li class="active"><?= l('subscribers') ?></li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title m-b-0"><?= l('subscribers') ?></h3>

                    <?php $this->load->view('includes/alerts') ?>
                    <div class="table-responsive">
                        <table id="datatable6" class="table table-striped">
                            <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th><?= l('msisdn') ?></th>
                                <th>Info</th>
                                <th>Command</th>
                                <th><?= l('date') ?></th>
                            </tr>
                            </thead>

                        </table>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->
    <?php $this->load->view('includes/footer') ?>
</div>
<!-- /#page-wrapper -->
</div>