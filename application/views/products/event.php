    <div id="page-wrapper">
      <div class="container-fluid">
        <div class="row bg-title">
          <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><?=l('products') ?></h4>
          </div>
          <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
              <li><a href="#"><?=l('dashboard') ?></a></li>
              <li><a href="#"><?=l('products') ?></a></li>
              <li class="active"><?=l('event') ?></li>
            </ol>
          </div>
          <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
          <div class="col-sm-12">
            <div class="white-box">
              <h3 class="box-title m-b-0"><?=l('event_module') ?></h3>
              
              <button type="button" class="btn btn-info pull-right btn-xs" data-toggle="modal" href='#modal'>
                <?=l('add_new') ?>
              </button>
              <br/><br/>
              <?php $this->load->view('includes/alerts') ?>
              <div class="table-responsive">
                <table id="datatable6" class="table table-striped">
                  <thead>
                    <tr>
                      <th>&nbsp;</th>
                      <th><?=l('code') ?></th>
                      <th><?=l('name') ?></th>
                      <th><?=l('price') ?></th>
                      <th><?=l('quantity') ?></th>
                      <th><?=l('action') ?></th>
                    </tr>
                  </thead>
                  </table>
                </div>
              </div>
            </div>

          </div>
          <!-- /.row -->

          <div class="modal fade" id="modal">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                  </button>
                  <h4 class="modal-title"><?=l('product_form') ?></h4>
                </div>
                <?=form_open(site_url('products/manage/event')) ?>
                <div class="modal-body">

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="" class="control-label"><?=l('product_name') ?></label>
                        <input type="text" class="form-control" id="product_name" name="product_name" value="<?=set_value('product_name') ?>">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="" class="control-label"><?=l('product_price') ?></label>
                        <input type="text" class="form-control" id="product_price" name="product_price" value="<?=set_value('product_price') ?>">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="" class="control-label"><?=l('quantity') ?></label>
                        <input type="text" class="form-control" id="quantity" name="quantity" value="<?=set_value('quantity') ?>">
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="" class="control-label"><?=l('description') ?></label>
                        <textarea name="desc" id="desc" class="form-control" rows="3" required>
                          <?=set_value('desc') ?>
                        </textarea>

                      </div>
                    </div>
                  </div>

                  <input type="hidden" name="product_type" value="event">
                  <input type="hidden" name="product_id" id="product_id" value="">
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal"><?=l('close') ?></button>
                  <button type="submit" class="btn btn-info"><?=l('save') ?></button>
                </div>
                <?=form_close() ?>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->
        <?php $this->load->view('includes/footer') ?>
      </div>
      <!-- /#page-wrapper -->
    </div>