<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row bg-title">
      <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">Products</h4>
      </div>
      <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
          <li><a href="#">Dashboard</a></li>
          <li><a href="#">Products</a></li>
          <li class="active">Events</li>
        </ol>
      </div>
      <!-- /.col-lg-12 -->
    </div>
    <!-- /row -->
    <div class="row">
      <div class="col-sm-12">
        <div class="white-box">
          <h3 class="box-title m-b-0">Events</h3>
          <button type="button" class="btn btn-info pull-right" data-toggle="modal" href='#modal'>Add New</button>
          <br/><br/>
          <div class="table-responsive">
            <table id="datatable" class="table table-striped">
              <thead>
                <tr>
                  <th>&nbsp;</th>
                  <th>Code</th>
                  <th>Name</th>
                  <th>Slots</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $count = 1; ?>
                <?php for($i = 0; $i<=30; $i++): ?>
                  <tr>
                    <td><?=$count++ ?></td>
                    <td>Tiger Nixon</td>
                    <td>System Architect</td>
                    <td>Edinburgh</td>
                    <td>
                      <div class="btn-group m-r-10 btn-sm">
                        <button aria-expanded="true" data-toggle="dropdown" class="btn btn-info dropdown-toggle waves-effect waves-light" type="button">Actions <span class="caret"></span></button>
                        <ul role="menu" class="dropdown-menu animated flipInX">
                          <li><a href="#">Edit</a></li>
                          <li class="divider"></li>
                          <li><a href="#">Delete</a></li>
                        </ul>
                      </div>
                    </td>
                  </tr>
                <?php endfor; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
    <!-- /.row -->

    <div class="modal fade" id="modal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Product Form</h4>
          </div>
          <div class="modal-body">
            <form action="" method="POST" role="form">

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="" class="control-label">Event Name</label>
                    <input type="text" class="form-control" id="" placeholder="Input field">
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="" class="control-label">Slot Price</label>
                    <input type="text" class="form-control" id="" placeholder="Input field">
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="" class="control-label">Slots</label>
                    <input type="text" class="form-control" id="" placeholder="Input field">
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="" class="control-label">Event Description</label>
                    <input type="text" class="form-control" id="" placeholder="Input field">
                  </div>
                </div>
              </div>


            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>

  </div>
  <!-- /.container-fluid -->
  <?php $this->load->view('includes/footer') ?>
</div>