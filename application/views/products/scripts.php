<script type="text/javascript">
	function edit(product_id = 0){
		url = site_url+'/products/manage';
		data = {'id':product_id};
		$.post(url, data, function(response){
			response = JSON.parse(response);
			$('#product_id').val(response.product_id);
			$('#product_name').val(response.product_name);
			$('#product_price').val(response.product_price);
			$('#quantity').val(response.quantity);
			$('#desc').val(response.desc);
			$('#modal').modal();
		});

		
	}

	$(document).ready(function(){
		var url = "<?=site_url('products/data') ?>";
		$('datatable-products').DataTable({
			"ajax":{"url":url},
		});
		$.get(url, function(data){
			//console.log(data);
		})
	})
</script>