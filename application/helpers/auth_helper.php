<?php

Class Auth
{
	static function isLoggedIn($type = MERCHANT)
	{
		$CI = & get_instance();
		
		if($CI->session->has_userdata('admin_id'))
			return true;

		if($CI->session->has_userdata('merchant_id'))
			return true;

		if($CI->session->has_userdata('agent_id'))
			return true;

		return false;
	}

	static function role($role_id = 1)
	{
		$roles = [
			0 => 'Guest',
			1 => 'Premium User',
			9 => 'Admin'
		];
		return $roles[$role_id];
	}


	static function logout($type = 'user')
	{
		$CI = & get_instance();
		unset($_SESSION['merchant_id']);
		unset($_SESSION['admin_id']);
		unset($_SESSION['agent_id']);
		unset($_SESSION['merchant_name']);
		unset($_SESSION['previous_url']);
		$CI->session->sess_destroy();

		return true;
	}

	static function login($data = []){

		$CI 		= & get_instance();
		$query 		= $CI->db->get_where(MERCHANTS, $data);
		$row 		= $query->row();

		if($query->num_rows() > 0){

			$data = ['last_login'=>date('Y-m-d H:i:s')];

			DB::update(MERCHANTS, ['id'=>$row->id],$data);

			$userdata = [

				'merchant_id'				=> $row->id,
				'merchant_status'			=> $row->status,
				'merchant_country_id' 		=> $row->country_id,
				'merchant_name'				=> $row->merchant_name,
				'user_lang'					=> $row->language,
                'parent'                    =>$row->is_parent

			];

			$CI->session->set_userdata($userdata);

			return true;

		}else{

			$CI->session->set_flashdata(ERROR, 'Invalid Username or Password');

			return false;
		}
	}

	static function loginAgent($data = []){

		$CI 		= & get_instance();
		$query 		= $CI->db->get_where('agents', $data);
		$row 		= $query->row();

		if($query->num_rows() > 0){

			$data = ['last_login' => date('Y-m-d H:i:s')];

			DB::update('agents', ['id' => $row->id], $data);

			$userdata = ['agent_id' => $row->id];

			$CI->session->set_userdata($userdata);

			return true;

		}else{

			$CI->session->set_flashdata(ERROR, 'Invalid Username or Password');

			return false;
		}
	}

	static function loginAdmin($data = []){

		$CI = & get_instance();
		$query = $CI->db->get_where(ADMINS, $data);
		$row = $query->row();

		if($query->num_rows() > 0){

			$data = ['last_login'=>date('Y-m-d H:i:s')];
			DB::update(ADMINS, ['admin_id'=>$row->admin_id],$data);

			if($row->status == DISABLED) {

				$CI->session->set_flashdata(ERROR, 'You are currently disabled.');

				return false;
			}
			
			$userdata = [

				'admin_id'		=>$row->admin_id,
				'admin_name'	=>$row->fullname,
				'admin_email'	=>$row->email,
				'user_lang'		=>$row->language
			];

			$CI->session->set_userdata($userdata);

			return true;

		}else{

			$CI->session->set_flashdata(ERROR, 'Invalid Username or Password');
			
			return false;
		}
	}

	static function bounce($uri_string = '')
	{
		$CI = & get_instance();
		$CI->session->set_userdata('previous_url', $uri_string);
		redirect('authentication');
	}

}