<?php


class DT
{


    static function mproducts()
    {

        return $order = ["url" => site_url('orders/datatable_json'),
            "order" => [8, 'desc'],
            "columnDefs" => [

                ["num", false, true, true],
                ["date_ordered", true, true, true],
                ["order_reference", true, true, true],
                ["msisdn", true, true, true],
                ["total", true, true, true],
                ["order_status", true, true, true],
                ["payment_date", true, true, true],
                ["action", false, false, true],
                ["order_id", true, true, false]

            ]
        ];

    }

    static function myMerchant()
    {
        return $merchant = ["url" => site_url('my_merchants/datatable_json'),
            "order" => [7, 'desc'],
            "columnDefs" => [
                ["num", false, true, true],
                ["merchant_name", true, true, true],
                ["merchant_phone", true, true, true],
                ["merchant_email", true, true, true],
                ["merchant_contact_person", true, true, true],
                ["wallet", true, true, true],
                ["action", false, false, true],
                ["id", false, false, false],

            ]
        ];
    }


    static function myMerchantOrder($id)
    {

        return $order = ["url" => site_url('my_merchants/m_json/' . $id),
            "order" => [8, 'desc'],
            "columnDefs" => [
                ["numt", false, true, true],
                ["date_ordered", true, true, true],
                ["order_reference", true, true, true],
                ["msisdn", true, true, true],
                ["total", true, true, true],
                ["order_status", true, true, true],
                ["payment_date", true, true, true],
                ["action", false, false, true],
                ["order_id", true, true, false]

            ]
        ];

    }

    static function products($type)
    {

        return $order = ["url" => site_url('products/product_json/' . $type),
            "order" => [6, 'desc'],
            "columnDefs" => [
                ["num", false, true, true],
                ["product_code", true, true, true],
                ["product_name", true, true, true],
                ["product_price", true, true, true],
                ["quantity", true, true, true],
                ["action", false, false, true],
                ["id", true, true, false]


            ]
        ];

    }

    static function customer()
    {

        return $order = ["url" => site_url('customers/customer_json/'),
            "order" => [1, 'desc'],
            "columnDefs" => [
                ["num", false, true, true],
                ["customer.name", true, true, true],
                ["orders.msisdn", true, true, true],
                ["action", false, false, true]


            ]
        ];

    }

    static function customerTransaction($msisdn)
    {

        return $order = ["url" => site_url('customers/transaction/' . $msisdn),
            "order" => [8, 'desc'],
            "columnDefs" => [

                ["numt", false, true, true],
                ["date_ordered", true, true, true],
                ["order_reference", true, true, true],
                ["msisdn", true, true, true],
                ["total", true, true, true],
                ["order_status", true, true, true],
                ["payment_date", true, true, true],
                ["action", false, false, true],
                ["order_id", true, true, false]

            ]
        ];
    }


    static function subscriber()
    {

        return $order = ["url" => site_url('products/subscriber_json'),
            "order" => [4, 'desc'],
            "columnDefs" => [

                ["numt", false, true, true],
                ["msisdn", true, true, true],
                ["log", true, true, true],
                ["custom_command", true, true, true],
                ["created_at", true, true, true]


            ]
        ];
    }
}



