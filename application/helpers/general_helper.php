<?php

use GuzzleHttp\Client;

function c($number = 0)
{
    if (s('currerncy') != null) {

        return s('currency') . ' ' . number_format((float)$number, 2);
    }

    $country = DB::first(COUNTRIES, ['id' => s('merchant_country_id')]);

    if ($country != null) {

        $CI = &get_instance();

        $CI->session->set_userdata('currency', $country->currency);

        return $country->currency . ' ' . number_format((float)$number, 2);

    }

    return '&#8358; ' . number_format((float)$number, 2);
}

function currency(){

    $country = DB::first(COUNTRIES, ['id' => s('merchant_country_id')]);

    if ($country != null) {

        $CI = &get_instance();

        $CI->session->set_userdata('currency', $country->currency);

        return $country->currency ;

    }

    return '&#8358; ' ;

}

function d($date = '', $isString = true)
{
    if (is_null($date) || empty($date))
        return '';

    if ($isString)
        return "<span class'text-muted'><i class='fa fa-clock-o'></i> " . date('Y-m-d g:i A', strtotime($date)) . "</span>";

    return "<span class'text-muted'><i class='fa fa-clock-o'></i> " . date('Y-m-d g:i A', $date) . "</span>";
}


function drop_down($table = '', $select_name = '', $value_field = '', $value_label = '', $selected = '', $select_type = 'selectboxit visible')
{
    $data = DB::get($table);
    $value_required = get_phrase('value_required');
    $select_phrase = get_phrase('select');
    $select = "<select name='{$select_name}' class='form-control {$select_type}'>";
    $select .= "<option value=''>{$select_phrase}</option>";
    foreach ($data as $row) {
        $label = ucfirst($row->$value_label);
        if ($row->$value_field == $selected) {
            $select .= "<option value='{$row->$value_field}' selected>{$label}</option>";
        } else {
            $select .= "<option value='{$row->$value_field}'>{$label}</option>";
        }

    }

    $select .= "</select>";

    return $select;
}


function s($userdata = '')
{
    $CI = &get_instance();
    return $CI->session->userdata($userdata);
}


function l($phrase = '')
{
    $language = s('user_lang') != '' ? s('user_lang') : 'english';
    $CI = &get_instance();
    $CI->lang->load($language, $language);
    if ($CI->lang->line($phrase) == '')
        return ucfirst(implode(' ', explode('_', $phrase)));
    return $CI->lang->line($phrase);

}


function logActivity($activity = '')
{
    $data['merchant_id'] = s('merchant_id');
    $data['activity'] = $activity;
    $data['created_at'] = date('Y-m-d H:i:s');
    DB::save(MERCHANT_ACTIVITIES, $data);
    return true;
}


function logAdminActivity($activity = '')
{
    $data['admin_id'] = s('admin_id');
    $data['activity'] = $activity;
    $data['created_at'] = date('Y-m-d H:i:s');
    DB::save(ADMIN_ACTIVITIES, $data);
    return true;
}


function uniqueRandomNumbersWithinRange($min = 0, $max = 0, $seed = 0)
{
    $numbers = range($min, $max);
    shuffle($numbers);

    return array_slice($numbers, 0, $seed);
}

function generateUniqueCode($requestId = 0, $seed = 0)
{
//hash the requestId with md5
    $md5Hash = md5($requestId);

//convert hex string to decimal
    $dec = strval(substr(number_format(hexdec($md5Hash), 2, '.', ''), 0, 32));

    $indices = uniqueRandomNumbersWithinRange(0, 31, $seed);
    $order_id = '';

    foreach ($indices as $index) {
        $order_id .= $dec[$index];
    }

    return $order_id;
}

function uniqueProductCode()
{
    $requestId = rand();
    $seed = 6;

    $reservedCodes = [000000, 111111, 222222, 333333, 444444, 555555, 666666, 777777, 88888, 99999];

    $productCode = generateUniqueCode($requestId, $seed);

    if (DB::itExists(PRODUCTS, 'product_code', $productCode))
        uniqueProductCode();

    if (in_array($productCode, $reservedCodes))
        uniqueProductCode();

    return $productCode;
}

function generateRegistraionCode()
{
    $requestId = rand();
    $seed = 8;
    $code = generateUniqueCode($requestId, $seed);

    if (DB::itExists(CODES, 'code', $code)) {

        generateRegistraionCode();
    }
    return $code;
}

function generateRegistrationCodes($count = 0)
{
    $codes = [];
    for ($i = 0; $i < $count; $i++) {
        $codes[$i] = generateRegistraionCode();
    }
    return $codes;
}

function isPasswordSameDigits($password)
{
    $check = [0, 101, 202, 303, 404, 505, 606, 707, 808, 909];
    if ((int)$password % 11 == 0 && in_array($password / 11, $check)) {
        return true;
    }

    return false;
}

function isNubanValid($input = '')
{

    $nuban = str_split($input);

    $total = ($nuban[0] * 3) + ($nuban[1] * 7) + ($nuban[2] * 3) + ($nuban[3] * 3) + ($nuban[4] * 7) + ($nuban[5] * 3) + ($nuban[6] * 3) + ($nuban[7] * 7) + ($nuban[8] * 3) + ($nuban[9] * 3) + ($nuban[10] * 7) + ($nuban[11] * 3);

    $check_digit = (10 - ($total % 10)) == 10 ? 0 : (10 - ($total % 10));

    if ($check_digit != $nuban[12]) {
        return false;
    }

    return true;
}

function sensorMsisdn($msisdn = '')
{
    for ($i = 5; $i < 9; $i++) {
        $msisdn[$i] = '*';
    }

    return $msisdn;
}

function dd($var = '')
{
    echo '<pre>';
    print_r($var);
    echo '</pre>';
    die;
}

function badge($status = '')
{
    if (empty(trim($status)))
        return '';

    $string = ucwords(str_replace('_', ' ', $status));

    if ($status == 'awaiting_payment')
        return "<span class='badge badge-warning'><i class='fa fa-exclamation-circle mr-5'></i> {$string}</span>";

    if ($status == 'completed' || $status == 'success' || $status == 'enabled')
        return "<span class='badge badge-success'><i class='fa fa-check mr-5'></i> {$string}</span>";

    if ($status == 'cancelled' || $status == 'cancelled_for_timeout' || $status == 'disabled')
        return "<span class='badge badge-danger'><i class='fa fa-times-circle mr-5'></i> {$string}</span>";

    return "<span class='badge badge-info'><i class='fa fa-clock-o mr-5'></i> {$string}</span>";
}


function k($status = '')
{
    if (empty(trim($status)))
        return '';

    $string = ucwords(str_replace('_', ' ', $status));

    if ($status == 'awaiting_payment')
        return $string;

    if ($status == 'completed' || $status == 'success' || $status == 'enabled')
        return $string;

    if ($status == 'cancelled' || $status == 'cancelled_for_timeout' || $status == 'disabled')
        return $string;

    return $string;
}


function api($type = '', $merchant_id = '')
{

    $CI = &get_instance();
    $endpointCall = '';

    if ($type == 'test') {

        $endpointCall = getenv('API_ENDPOINT_STAGING');

    } else if ($type == 'live') {

        $endpointCall = getenv('API_ENDPOINT');

    }


    try {

        $endpoint = $endpointCall . '/' . $merchant_id . '/api/token';


        $client = new Client([

            'headers' => [

                'token' => getenv('API_TOKEN'),
                'publickey' => hash('sha512', getenv('API_TOKEN') . getenv('API_PRIVATE_KEY'))
            ]
        ]);

        $response = $client->request('GET', $endpoint);

        $response = json_decode($response->getBody());

        if ($response->status == 'success') {

            $apiData = [
                'api_token' => $response->data->token,
                'private_key' => $response->data->private_key

            ];
            return $apiData = (object)$apiData;

        } else {
            return $apiData = (object)$apiData = [];
        }


    } catch (Exception $e) {

        $CI->session->set_flashdata(ERROR, 'Not Found');

    }

}


function generateApi($type = '', $merchant_id = '', $country = '')
{

    $CI = &get_instance();
    $endpointCall = '';

    if ($type == 'test') {

        $endpointCall = getenv('API_ENDPOINT_STAGING');


    } else if ($type == 'live') {

        $endpointCall = getenv('API_ENDPOINT');

    }


    try {

        $endpoint = $endpointCall . '/' . $merchant_id . '/api/token/generate/' . $country;

        $client = new Client([

            'headers' => [

                'token' => getenv('API_TOKEN'),
                'publickey' => hash('sha512', getenv('API_TOKEN') . getenv('API_PRIVATE_KEY'))
            ]
        ]);

        $response = $client->request('GET', $endpoint);

        $response = json_decode($response->getBody());


        if ($response->status == 'success') {

            $CI->session->set_flashdata(SUCCESS, 'Token generated successfully');

            $apiData = [
                'api_token' => $response->data->token,
                'private_key' => $response->data->private_key

            ];
            return $apiData = (object)$apiData;

        } else {

            $CI->session->set_flashdata(ERROR, $response->message);

            return $apiData = (object)$apiData = [];
        }


    } catch (Exception $e) {

        $CI->session->set_flashdata(ERROR, 'Not Found');

    }


}






