<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code



//User Defined

// Used all over the application
define('APP_VERSION','1.0.0');
define('APP_NAME','OGARANYA');


//Define Database Tables
define('COUNTRIES','country');
define('CUSTOMERS','customer');
define('CUSTOMER_SEARCH','customer_search');
define('LANGUAGES', 'languages');
define('LOCATIONS', 'locations');
define('MERCHANTS', 'merchant');
define('MERCHANT_ORDERS', 'merchant_order');
define('MERCHANTS_ACCOUNT_DETAILS', 'account_details');
define('ORDERS', 'orders');
define('PAYMENT_GATEWAYS', 'payment_gateway');
define('PRODUCTS', 'product');
define('MERCHANT_WITHDRAWALS', 'withdrawals');
define('MERCHANT_ACTIVITIES', 'merchant_activities');
define('ADMINS', 'admins');
define('ADMIN_ACTIVITIES', 'admin_activities');
define('CODES', 'registration_codes');
define('CUSTOM_INSERT', 'custom_log');
define('BANKS', 'banks');
define('ONLINE_PAYMENT', 'online_payment');
define('TRANSACTIONS', 'transactions');
define('CUSTOM_COMMAND', 'custom_command');
define('API_AGENTS', 'api_agents');


// Other general constants
define('SUCCESS', 'success');
define('ERROR', 'error');
define('MESSAGE', 'message');
define('PRODUCT', 'product');
define('EVENT', 'event');
define('DONATION', 'donation');
define('UPLOADED_ERROR_LOG_PATH', './assets/tmp/errors/');
define('ENCRYPTION_PASSWORD', 'x');
define('COMPLETED', 'completed');
define('PENDING', 'pending');
define('ARCHIVED', 'archived');
define('ENABLED', 'enabled');
define('DISABLED', 'disabled');
define('PAID', 'paid');
define('ADMIN', 'admin');
define('MERCHANT', 'merchant');
define('ADMIN_CODE', 'a21232f297a57a5a743894a0e4a801fc3');
define('MIN_WITHDRAWAL_VALUE', 2000);
define('USED', 'used');
define('UNUSED', 'unused');
define('STARTED', 'started');
