<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class GeneralModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();

    }

    public function getMyMerchant($where = [], $merchantId = 0, $sortBy = 'id', $sortFormat = 'DESC', $limit = 0)
    {
        $_merchantId = $merchantId == 0 ? s('merchant_id') : $merchantId;

        if (count($where) > 0)
            $this->db->where($where);

        $this->db->where([

            MERCHANT . '.parent_id' => $_merchantId,


        ])->order_by(MERCHANT . '.' . $sortBy, $sortFormat);

        if ($limit != 0) $this->db->limit($limit);

        return $this->db->get(MERCHANT)->result();


    }

    public function getAllMerchantsAndParent($merchantId = 0)
    {


        $this->db->select('id');
        $this->db->where([MERCHANT . '.id' => $merchantId]);
        $this->db->or_where([MERCHANT . '.parent_id' => $merchantId]);
        $query = $this->db->get('merchant');
        foreach ($query->result() as $row) {

            $merchantIds[] = '' . $row->id . '';
        }
        return $mlist = implode(', ', $merchantIds);

    }

    public function getMerchantOrders($where = [], $merchantId = 0, $sortBy = 'order_id', $sortFormat = 'DESC', $limit = 0)
    {

        $_merchantId = $merchantId == 0 ? s('merchant_id') : $merchantId;

        $mlist = $this->getAllMerchantsAndParent($_merchantId);

        if (count($where) > 0)
            $query = $this->db->where($where);

        $this->db->where([ORDERS . '.order_status !=' => ARCHIVED]);
        $this->db->where_in(ORDERS . '.merchant_id', $mlist, false)->order_by(ORDERS . '.' . $sortBy, $sortFormat);

        if ($limit != 0) $this->db->limit($limit);

        return $this->db->get(ORDERS)->result();

    }

    public function getSingleMerchantOrder($order_id = 0, $where = [], $_val = 0)
    {


        $this->db->join(MERCHANT, MERCHANT . '.id = ' . ORDERS . '.merchant_id ');
        $this->db->join(CUSTOMERS, CUSTOMERS . '.msisdn = ' . ORDERS . '.msisdn');


        if ($_val == 1) {

            $this->db->where([

                ORDERS . '.order_id' => $order_id,

            ]);

        } else {

            if (count($where) > 0)
                $this->db->where($where);

            $this->db->where([

                ORDERS . '.order_id' => $order_id,

            ]);

        }


        return $this->db->get(ORDERS)->row();

    }

    public function merchantOrdersCount($merchantId = 0)
    {
        $_merchantId = $merchantId == 0 ? s('merchant_id') : $merchantId;

        $mlist = $this->getAllMerchantsAndParent($_merchantId);


        $this->db->where([ORDERS . '.order_status =' => COMPLETED]);
        $this->db->where_in(ORDERS . '.merchant_id', $mlist, false);

        return $this->db->get(ORDERS)->num_rows();
    }

    public function merchantOrdersAmount($merchantId = 0)
    {
        $_merchantId = $merchantId == 0 ? s('merchant_id') : $merchantId;

        $mlist = $this->getAllMerchantsAndParent($_merchantId);


        $this->db->where([ORDERS . '.order_status =' => COMPLETED]);
        $this->db->where_in(ORDERS . '.merchant_id', $mlist, false);
        $this->db->select_sum('total');
        $query=$this->db->get(ORDERS);
      
        return  $query->row()->total;
    }



    public function merchantGrandBalance($merchantId = 0)
    {
        $_merchantId = $merchantId == 0 ? s('merchant_id') : $merchantId;

        $sum = 0.0;


        $this->db->where([MERCHANT . '.id' => $_merchantId,]);

        $row = $this->db->get(MERCHANT)->row();

        $sum = $row->wallet;

        return $sum;


    }

    public function merchantTotalWithdrawal($status = '', $merchantId = 0)

    {

        $_merchantId = $merchantId == 0 ? s('merchant_id') : $merchantId;


        if ($status == '')

            $where = ['member_id' => $_merchantId];

        else

            $where = ['member_id' => $_merchantId, 'transaction_type' => $status];


        return DB::tableSum(TRANSACTIONS, 'amount', $where);

    }


    public function merchantTotalWithdrawals($status = '', $merchantId = 0)
    {

        $_merchantId = $merchantId == 0 ? s('merchant_id') : $merchantId;

        $mlist = $this->getAllMerchantsAndParent($_merchantId);

        $sum = 0.0;


        if ($status == '')

            $this->db->where([TRANSACTIONS . '.member_id' => $_merchantId,]);
        else
            $this->db->where([TRANSACTIONS . '.transaction_type' => $status]);

        $this->db->where_in(TRANSACTIONS . '.member_id', $mlist, false);


        $allMerchantOrders = $this->db->get([TRANSACTIONS])->result();

        foreach ($allMerchantOrders as $row)
            $sum += $row->amount;

        return $sum;
        // print_r($sum); exit;

    }


    public function merchantWalletBalance($merchantId = 0)
    {


        $_merchantId = $merchantId == 0 ? s('merchant_id') : $merchantId;

        $this->db->where([

            MERCHANT . '.id' => s('merchant_id')

        ]);


        return $this->db->get(MERCHANT)->row()->wallet;

    }

    public function highestMerchantOrder($merchantId = 0)
    {
        $_merchantId = $merchantId == 0 ? s('merchant_id') : $merchantId;

        $mlist = $this->getAllMerchantsAndParent($_merchantId);

        $this->db->where_in(ORDERS . '.merchant_id', $mlist, false);

        $this->db->where(ORDERS . '.order_status', 'completed');

        $this->db->select_max('total', 'max');

        return $this->db->get(ORDERS)->result()[0]->max;
    }

    public function getMerchantCustomers()
    {
        $merchantOrders = $this->getMerchantOrders($where = [], s('merchant_id'));
        $customers = [];

        foreach ($merchantOrders as $row)
            $customers[] = $row->msisdn;

        return array_unique($customers);
    }


    public function getMerchantWalletActivities()
    {
        $where = ['member_id' => s('merchant_id')];
        $merchantWithdrawals = DB::get(MERCHANT_WITHDRAWALS, 'withdrawal_id', 'DESC', 20, 0, $where);
        $merchantCompletedOrders = $this->getMerchantOrders(['order_status' => 'completed'], s('merchant_id'));

        $activities = [];
        $i = 0;

        foreach ($merchantWithdrawals as $row) {
            $activities[$i]['date'] = strtotime($row->created_at);
            $activities[$i]['description'] = 'Withdrawal';
            $activities[$i]['credit'] = '';
            $activities[$i]['debit'] = c($row->amount);
            $activities[$i]['class'] = 'text-danger';
            $i++;
        }

        foreach ($merchantCompletedOrders as $row) {
            $activities[$i]['date'] = strtotime($row->date_ordered);
            $activities[$i]['description'] = 'Order Completed [order ID: ' . $row->order_reference . ']';
            $activities[$i]['credit'] = c($row->total);
            $activities[$i]['debit'] = '';
            $activities[$i]['class'] = 'text-success';
            $i++;
        }

        if (count($activities) > 0)
            array_multisort($activities, SORT_DESC);

        return $activities;
    }

    public function getCompletedTransactionsCount()
    {
        return DB::count(ORDERS, ['order_status' => COMPLETED]);
    }

    public function getUniqueCustomersCount()
    {
        return DB::count(CUSTOMERS);
    }

    public function getMerchantOrderJson($where = [], $merchantId = 0, $sortBy = 'order_id', $sortFormat = 'DESC', $limit = 0)
    {

        $_merchantId = $merchantId == 0 ? s('merchant_id') : $merchantId;

        $mlist = $this->getAllMerchantsAndParent($_merchantId);


        $wh = array();
        if (count($where) > 0) {
            foreach ($where as $key => $val) {
                $wh[] = '' . $key . '  = "' . $val . '" ';
            }
        }


        $wh[] = '' . ORDERS . '.order_status !=  "' . ARCHIVED . '" ';

        $wh[] = ' ' . ORDERS . '.merchant_id IN  (' . $mlist . ')';

        $SQL = 'SELECT * FROM  ' . ORDERS . ' ';

        $GROUP_BY = '';

        if (count($wh) > 0) {
            $WHERE = implode(' and ', $wh);
            return $this->datatable->LoadJson($SQL, $WHERE, $GROUP_BY);

        } else {
            return $this->datatable->LoadJson($SQL);
        }


    }

    public function getMyMerchantJson($where = [], $merchantId = 0, $sortBy = 'id', $sortFormat = 'DESC', $limit = 0)
    {

        $_merchantId = $merchantId == 0 ? s('merchant_id') : $merchantId;

        $wh = array();

        if (count($where) > 0)
            $wh[] = ($where);

        $wh[] = '' . MERCHANT . '.parent_id = " ' . $_merchantId . '" ';

        $SQL = 'SELECT * FROM  ' . MERCHANT . ' ';

        $GROUP_BY = '';

        if (count($wh) > 0) {
            $WHERE = implode(' and ', $wh);
            return $this->datatable->LoadJson($SQL, $WHERE, $GROUP_BY);

        } else {
            return $this->datatable->LoadJson($SQL);
        }


    }


    public function getProductJson($type)
    {

        $wh = array();

        $wh[] = '' . PRODUCTS . '.merchant_id =  "' . s('merchant_id') . '" ';

        $wh[] = '' . PRODUCTS . '.product_type =  "' . $type . '" ';

        $SQL = 'SELECT * FROM  ' . PRODUCTS . ' ';

        $GROUP_BY = '';

        if (count($wh) > 0) {
            $WHERE = implode(' and ', $wh);
            return $this->datatable->LoadJson($SQL, $WHERE, $GROUP_BY);

        } else {
            return $this->datatable->LoadJson($SQL);
        }

    }

    public function getMerchantCustomersJson()
    {


        $wh = array();

        $mlist = $this->getAllMerchantsAndParent(s('merchant_id'));

        $wh[] = '' . ORDERS . '.order_status !=  "' . ARCHIVED . '" ';

        $wh[] = ' ' . ORDERS . '.merchant_id IN  (' . $mlist . ')';

        $SQL = 'SELECT DISTINCT ' . ORDERS . '.msisdn,   ' . CUSTOMERS . '.name  FROM  ' . ORDERS . ' JOIN ' . CUSTOMERS . ' ON 
          ' . ORDERS . '.`msisdn` = ' . CUSTOMERS . '.`msisdn` ';

        $GROUP_BY = '';

        if (count($wh) > 0) {
            $WHERE = implode(' and ', $wh);
            return $this->datatable->LoadJson($SQL, $WHERE, $GROUP_BY);

        } else {
            return $this->datatable->LoadJson($SQL);
        }

    }


    public function getCustomLogJson()
    {

        $wh = array();

        $wh[] = '' . CUSTOM_INSERT . '.merchant_id  =  "' . s('merchant_id') . '" ';

        $SQL = 'SELECT *   FROM  ' . CUSTOM_INSERT . ' JOIN ' . CUSTOM_COMMAND . ' ON  ' . CUSTOM_INSERT .
            '.`custom_command_id` = ' . CUSTOM_COMMAND . '.`id` ';

        $GROUP_BY = '';

        if (count($wh) > 0) {
            $WHERE = implode(' and ', $wh);
            return $this->datatable->LoadJson($SQL, $WHERE, $GROUP_BY);

        } else {
            return $this->datatable->LoadJson($SQL);
        }

    }


}
