<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();

        if (!Auth::isLoggedIn())
            Auth::bounce($this->uri->uri_string());
        $this->load->helpers('dt_helper');
        $this->load->helpers('general_helper');

    }

    public function index($type = 'product')
    {

        $page_data['title'] = 'products';
        $page_data['page'] = 'products/' . $type;
        $page_data['scripts'] = 'products/scripts';
        $page_data['products'] = DT::products($type);
        $this->load->view('template', $page_data);
    }

    function product_json($type)
    {
        $data = array();

        $Records = $this->generalModel->getProductJson($type);

        $button = '';

        $v = 1;

        foreach ($Records['data'] as $r) {

            $prodId = $r['id'];
            $button = '       <div class="btn-group m-r-10 btn-xs">
            <button aria-expanded="true" data-toggle="dropdown" class="btn btn-info dropdown-toggle waves-effect waves-light" type="button">Actions <span class="caret"></span></button>
            <ul role="menu" class="dropdown-menu animated flipInX">
              <li><a href="javascript:void(0);" onclick="edit(' . $prodId . ')">Edit</a></li>
              <li class="divider"></li>
              <li>
                <a href="#" onclick="return confirm(\'Are you sure you want to delete this product?\')">Delete</a>
              </li>
            </ul>
          </div>';

            $data [] = array(
                $v,
                $r['product_code'],
                $r['product_name'],
                $r['product_price'],
                $r['quantity'],
                $button,
                $r['id']


            );
            $v++;

        }

        $Records['data'] = $data;

        echo json_encode($Records);
    }

    public function data($type = 'product')
    {
        $where = ['merchant_id' => s('merchant_id'), 'product_type' => $type];

        $products = DB::getArray(PRODUCTS, 'product_name', 'ASC', 0, 0, $where);

        $additional_data = [];

        for ($i = 0; $i < count($products); $i++) {

            $edit = "<li><a href='javascript:void(0)' onclick='edit('" . $products[$i]['id'] . "')'>Edit</a></li>";
            $action = "<div class='btn-group m-r-10 btn-sm'>";
            $action .= "<button aria-expanded='true' data-toggle='dropdown' class='btn btn-info btn-sm dropdown-toggle waves-effect waves-light' type='button'>Actiions <span class='caret'></span></button>";
            $action .= "<ul role='menu' class='dropdown-menu animated flipInX'>";
            $action .= $edit;
            $action .= "</ul>";
            $action .= "</div>";

            $additional_data[$i]['action'] = $action;
        }

        $new_data = [];
        $i = 0;

        for ($i = 0; $i < count($products); $i++) {

            $new[$i]['product_code'] = $products[$i]['product_code'];
            $new[$i]['product_name'] = $products[$i]['product_name'];
            $new[$i]['product_price'] = $products[$i]['product_price'];
            $new[$i]['quantity'] = c($products[$i]['quantity']);
            $new[$i]['action'] = $additional_data[$i];
            $i++;
        }

        unset($i, $products, $additional_data);

        $output = [];

        foreach ($new as $key => $value) {
            $output[] = array_values($value);
        }

        echo json_encode(['data' => $output], JSON_HEX_QUOT | JSON_HEX_TAG);
    }

    public function manage($type = 'product')
    {

        if ($this->input->is_ajax_request()) {
            if ($row = DB::getRow(PRODUCTS, ['id' => $this->input->post('id')])) {
                $product = [

                    'product_id' => $row->id,
                    'product_code' => $row->product_code,
                    'product_name' => $row->product_name,
                    'product_price' => $row->product_price,
                    'quantity' => $row->quantity,
                    'desc' => $row->desc,
                ];

                echo json_encode($product);
                exit;
            }
        }


        if (!$this->input->post())
            redirect('products');

        $data = $this->input->post();

        if ($this->input->post('product_id')) {

            $where = ['id' => $data['product_id']];
            unset($data['product_id']);

            $this->form_validation->set_rules('product_price', 'Product Price', 'required|numeric');
            $this->form_validation->set_rules('quantity', 'Quantity', 'required|numeric');
            $this->form_validation->set_rules('desc', 'Description', 'required');

            if ($this->form_validation->run()) {

                DB::update(PRODUCTS, $where, $data);

                $this->session->set_flashdata(SUCCESS, l('action_succesful'));
                logActivity('Product :[' . $data['product_name'] . '] Updated');

            } else {

                $this->session->set_flashdata(ERROR, validation_errors());
                redirect('products');
            }

        } else {

            $this->form_validation->set_rules('product_name', 'Product Name', 'required|is_unique[' . PRODUCTS . '.product_name]');
            $this->form_validation->set_rules('product_price', 'Product Price', 'required|numeric');
            $this->form_validation->set_rules('quantity', 'Quantity', 'required|numeric');
            $this->form_validation->set_rules('desc', 'Description', 'required');

            if ($this->form_validation->run()) {

                $data['merchant_id'] = s('merchant_id');
                $data['country_id'] = DB::getCell(MERCHANTS, ['id' => s('merchant_id')], 'country_id');
                $data['product_code'] = uniqueProductCode();

                unset($data['product_id']);

                DB::save(PRODUCTS, $data);
                logActivity('New product added. [Code: ' . $data['product_code'] . ']');
                $this->session->set_flashdata(SUCCESS, l('action_succesful'));

            } else {

                $this->session->set_flashdata(ERROR, validation_errors());
            }
        }

        switch ($type) {
            case EVENT:
                redirect('products/index/event');
                break;

            case PRODUCT:
                redirect('products/index/product');
                break;

            default:
                redirect('products');
                break;
        }

    }

    public function delete($id = '')
    {

        $where = ['id' => $id];
        if (DB::delete(PRODUCTS, $where)) {
            logActivity('Product Deleted. [ID: ' . $id . ']');
            $this->session->set_flashdata(SUCCESS, l('action_succesful'));
        } else {
            $this->session->set_flashdata(ERROR, l('action_unsuccesful'));
        }
        rediret('products');
    }

    public function sample_page()
    {
        $this->load->helper('download');
        force_download(FCPATH . 'assets/tmp/products.xlsx', NULL);
        echo "<script>window.close();</script>";
    }

    public function subscribers()
    {
        $page_data['title'] = l('subscribers');
        $page_data['page'] = 'products/subscribers';
        $page_data['products'] = DT::subscriber();
        //$page_data['subscribers'] = DB::get(CUSTOM_INSERT, 'created_at', 'DESC', 0, 0, ['merchant_id' => s('merchant_id')]);

        $this->load->view('template', $page_data);
    }


    function subscriber_json()
    {
        $data = array();

        $Records = $this->generalModel->getCustomLogJson();


        $button = '';

        $v = 1;

        foreach ($Records['data'] as $r) {

            $data [] = array(
                $v,
                $r['msisdn'],
                $r['log'],
                $r['custom_command'],
                d($r['created_at'])

            );
            $v++;

        }

        $Records['data'] = $data;

        echo json_encode($Records);
    }

    public function bulk()
    {

        $this->load->helper('file');
        $this->load->library('PHPExcel/PHPExcel');
        $this->load->library('PHPExcel/PHPExcel/Cell/PHPExcel_Cell_AdvancedValueBinder');

        $config['upload_path'] = './assets/tmp/';
        $config['allowed_types'] = 'xls|xlsx';
        $config['max_size'] = '6000';
        $config['file_name'] = time();

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('userfile')) {

            $this->session->set_flashdata(ERROR, $this->upload->display_errors());
            redirect('products');

        } else {

            $file_name = $this->upload->file_name;

            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
            $objReader->setReadDataOnly(true);

            $objPHPExcel = $objReader->load($this->upload->upload_path . $this->upload->file_name);
            $objWorksheet = $objPHPExcel->getActiveSheet();
            $highestRow = $objWorksheet->getHighestRow();

            $error = '';

            //error file configuration
            $error_log_filename = $config['file_name'] . ".txt";
            $error_count = $success_count = 0;

            $country_id = DB::getCell(MERCHANTS, ['id' => s('merchant_id')], 'country_id');

            //get all header key
            for ($row = 2; $row <= $highestRow; ++$row) //$highestRow
            {
                $product_name = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
                $product_price = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
                $quantity = trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
                $desc = trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue());

                //validations
                if (
                    empty($product_name) ||
                    empty($product_price) ||
                    empty($quantity) ||
                    empty($desc)
                ) {

                    $error = "Data in this row  is incomplete. product name =  {$product_name} \n";
                    write_file(UPLOADED_ERROR_LOG_PATH . $error_log_filename, $error, 'a+');
                    $error_count++;
                    continue;
                }

                if (DB::itExists(PRODUCTS, 'product_name', $product_name)) {

                    $error = "This product has previously been uploaded. Product name already exist. product name =  {$product_name} \n";
                    write_file(UPLOADED_ERROR_LOG_PATH . $error_log_filename, $error, 'a+');
                    $error_count++;
                    continue;
                }

                if (!is_numeric($quantity)) {

                    $error = "Quantity provided for this product is not numeric. product name = {$product_name}\n";
                    write_file(UPLOADED_ERROR_LOG_PATH . $error_log_filename, $error, 'a+');
                    $error_count++;
                    continue;

                }

                $data['merchant_id'] = s('merchant_id');
                $data['country_id'] = $country_id;
                $data['product_code'] = uniqueProductCode();
                $data['product_name'] = $product_name;
                $data['product_price'] = $product_price;
                $data['quantity'] = $quantity;
                $data['product_type'] = PRODUCT;
                $data['desc'] = $desc;


                if ($insert_id = DB::save(PRODUCTS, $data)) {
                    $success_count++;
                }

            } // End of for loop.

            $this->session->set_flashdata('success_count', 'There were ' . $success_count . ' succesful uploads.');

            if ($error_count > 0) {

                $link = base_url('assets/tmp/errors/' . $error_log_filename);
                $this->session->set_flashdata('link', 'click <a href="' . $link . '" target="_blank">here</a> to view the error log.');
            }

            logActivity('Bulk products upload made.');
            redirect('products');
        }

    }


}
