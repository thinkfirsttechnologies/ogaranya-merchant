<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['page']		= 'site/index';
		$data['title']		= 'Ogaranya - Home';

		$this->load->view('site/template', $data);

	}

	public function about()
	{
		$data['page']		= 'site/about';
		$data['title']		= 'Ogaranya - About';

		$this->load->view('site/template', $data);
	}

	public function how()
	{
		$data['page']		= 'site/how';
		$data['title']		= 'Ogaranya - How it works';

		$this->load->view('site/template', $data);
	}

	public function channels()
	{
		$data['page']		= 'site/channels';
		$data['title']		= 'Ogaranya - Channels';

		$this->load->view('site/template', $data);
	}

	public function contact()
	{

		if($this->input->post()){


			try{

				$payload = [

					'secret' 		=> '6LehBJIUAAAAAJM5hwMzEBUJ_g_wkETUts1glTe3',
					'response'		=> $this->input->post('recaptcha'),
					'remoteip'		=> $this->input->ip_address()
				];

				$client = new \GuzzleHttp\Client();

				$response = $client->post('https://www.google.com/recaptcha/api/siteverify', [

					'content-type' 	=> 'application/x-www-form-urlencoded',
					'form_params'	=> $payload
				]);

				$body  = json_decode($response->getBody());

				if(isset($body->success) && $body->success == true && isset($body->score) && $body->score > 0.5){


					$email['page']      = 'contact';
					$email['name']		= $this->input->post('name');
					$email['email']		= $this->input->post('email');
					$email['message']	= html_entity_decode($this->input->post('message'));
					$email['body']		= $this->input->post('message');

					$body       		= $this->load->view('emails/template', $email, true);

					Notification::sendEmail('support@ogaranya.com', 'New Contact Message', $body);

					$data['message'] = 'We are glad to hear from you! We will be in touch very soon.';
				
				}else{

					$data['message'] = 'Please try again. we suspect you are a Robot.';
				}
				

			}catch(Exception $e){

				$data['message'] = 'Please try again. we suspect you are a Robot.';
			}
			
		}

		$data['page']		= 'site/contact';
		$data['title']		= 'Ogaranya - Contact Us';

		$this->load->view('site/template', $data);
	}

	public function form()
	{		
		$this->load->helper('download');

		force_download('Ogaranya-Merchant-Forms.pdf', file_get_contents(FCPATH.'assets/files/Forms.pdf'));

		echo "<script>window.close()</script>";
	}
}