<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!Auth::isLoggedIn(ADMIN))
			Auth::bounce($this->uri->uri_string());
		}

		public function index()
		{
			$data['scripts'] 			= 'admin/dashboard/scripts';
			$data['title']				= 'dashboard';
			$data['page']				= 'admin/dashboard/index';
			$data['totalMerchants']		= DB::numRows(MERCHANTS);
			$data['transCount']			= $this->generalModel->getCompletedTransactionsCount();
			$data['pendingRequests']	= DB::numRows(MERCHANT_WITHDRAWALS, ['status'=>PENDING]);
			$data['uniqueCustomers']	= $this->generalModel->getUniqueCustomersCount();
			$where 							= ['admin_id'=>s('admin_id')];
			$data['userActivity']		= DB::get(ADMIN_ACTIVITIES, 'activity_id', 'DESC', 5, 0, $where);

			$this->load->view('template', $data);
		}

		public function merchants()
		{
			$data['title'] 		= 'merchants';
			$data['page']			= 'admin/merchants/manage';
			$data['merchants']		= DB::get(MERCHANTS, 'id', 'DESC', 0, 0, ['is_parent'=>0]);
			$this->load->view('template', $data);
		}

		public function new_merchant()
		{

			if ($this->input->post()) {

				$data = $this->input->post();

				$this->form_validation->set_rules('merchant_name', 'Store Name', 'required|alpha_numeric_spaces|is_unique['.MERCHANTS.'.merchant_name]');
				$this->form_validation->set_rules('merchant_phone', 'Phone Number', 'required|is_unique['.MERCHANTS.'.merchant_phone]');
				$this->form_validation->set_rules('merchant_email', 'Email', 'required|valid_email|is_unique['.MERCHANTS.'.merchant_email]');
				$this->form_validation->set_rules('merchant_address', 'Address', 'required');
				$this->form_validation->set_rules('merchant_contact_person', 'Contact Person', 'required');

				if ($this->form_validation->run()) {

					unset($data['id']);
					$country_prefix = DB::getCell(COUNTRIES, ['id'=>$data['country_id']], 'country_prefix');
					$data['merchant_phone']	= $country_prefix.substr($data['merchant_phone'], -10);

					$this->load->helper('string');
					$password 				= random_string('alnum', 15);
					$data['password']		= hash('sha512', $password);
					$data['status']			= ENABLED;
					$data['date_added']		= date('Y-m-d H:i:s');

					DB::save(MERCHANTS, $data);

					$email_data['page']					= 'merchant_autoloaded';
					$email_data['merchant_name']		= $data['merchant_name'];
					$email_data['merchant_email']		= $data['merchant_email'];
					$email_data['merchant_password']	= $password;
					$body 								= $this->load->view('emails/template', $email_data, TRUE);

					Notification::sendEmail($data['merchant_email'], 'Welcome to Ogaranya, '.$data['merchant_name'].'.!', $body);

					logAdminActivity('New merchant created. [Name: '.$data['merchant_name'].']');

					$this->session->set_flashdata(SUCCESS, 'Merchant created succesfully.');

					redirect('admin/merchants');

				}

			}


			$this->merchant_form();
		}

		public function merchant_form()
		{
			$data['title'] 		= 'new merchant';
			$data['page']			= 'admin/merchants/form';

			$this->load->view('template', $data);
		}

		public function edit_merchant($merchant_id = 0)
		{
			if($this->input->post()){

				$where = ['id'=>$merchant_id];

				$this->form_validation->set_rules('merchant_name', 'Store Name', 'required|alpha_numeric_spaces');
				$this->form_validation->set_rules('merchant_phone', 'Phone Number', 'required');
				$this->form_validation->set_rules('merchant_email', 'Email', 'required|valid_email');
				$this->form_validation->set_rules('merchant_address', 'Address', 'required');
				$this->form_validation->set_rules('merchant_contact_person', 'Contact Person', 'required');

				if($this->form_validation->run()){

					DB::update(MERCHANTS, $where, $data);

					$this->session->set_flashdata(SUCCESS, ACTION_SUCCESFUL);

					logAdminActivity('Merchant Information Updated :['.$data['merchant_name'].']');
					
				}else{

					redirect('admin/manage_merchant');
				}

			}else{

				$data['title'] 		= 'manage merchant';
				$data['page']			= 'admin/merchants/form';
				$data['merchant'] 		= DB::getRow(MERCHANTS, ['id'=>$merchant_id]);

				$this->load->view('template', $data);
			}
		}

		public function merchant_status($merchantId = 0, $status = '')
		{
			$data 		= ['status'=>$status];
			$merchant 	= DB::getRow(MERCHANTS, ['id'=>$merchantId]);

			DB::update(MERCHANTS,['id'=>$merchantId], $data);

			$data['merchant_name'] 	= $merchant->merchant_name;
			$data['status'] 		= $status;
			$data['page']			= 'merchant_status_change';
			$message 				= $this->load->view('emails/template', $data, TRUE);

			Notification::sendEmail($merchant->merchant_email, 'Your Account Status on Ogaranya has changed', $message);
			
			$this->session->set_flashdata(SUCCESS, l('action_succesful'));
			logAdminActivity('Merchant ['.$merchant->merchant_name.'] status changed to '.$status);

			redirect('admin/merchants');
		}

		public function merchant_impersonate($merchantId = 0)
		{
			$merchant = DB::first(MERCHANTS, ['id'=>$merchantId]);

			logAdminActivity($merchant->merchant_name.' Impersonated.');

			$this->session->set_userdata('logged_in_admin', s('admin_id'));
			unset($_SESSION['admin_id']);

			$this->session->set_userdata('merchant_id', $merchantId);
			$this->session->set_userdata('impersonate', 'You are Currently Impersonating '.$merchant->merchant_name);
			
			redirect('home');
		}

		public function products()
		{

			$data['title'] 		= 'products';
			$data['page']			= 'admin/products/manage';
			$data['products'] 		= DB::get(PRODUCTS, 'product_name', 'ASC');

			$this->load->view('template', $data);
		}

		public function withdrawals()
		{

			$data['title'] 		= 'merchant withdrawals';
			$data['page']			= 'admin/withdrawals/manage';
			$data['withdrawals'] 	= DB::get(MERCHANT_WITHDRAWALS, 'withdrawal_id', 'DESC', 0, 0, ['status'=>PENDING]);

			$this->load->view('template', $data);	
		}

		public function withdrawals_paid()
		{

			$data['title'] 		= 'merchant withdrawals';
			$data['page']			= 'admin/withdrawals/paid';
			$data['withdrawals'] 	= DB::get(MERCHANT_WITHDRAWALS, 'withdrawal_id', 'DESC', 0, 0, ['status'=>PAID]);

			$this->load->view('template', $data);	
		}

		public function withdrawal_approve()
		{
			if(!$this->input->post())
				redirect('admin/withdrawals');

			$withdrawalId = $this->input->post('withdrawal_id');

			$data 		= ['status'=>PAID, 'admin_id'=>s('admin_id'), 'approved_at'=>date('Y-m-d H:i:s')];
			$withdrawal = DB::first(MERCHANT_WITHDRAWALS, ['withdrawal_id'=>$withdrawalId]);

			DB::update(MERCHANT_WITHDRAWALS,['withdrawal_id'=>$withdrawalId], $data);

			$merchant 	= DB::first(MERCHANTS, ['id'=>$withdrawal->merchant_id]); 

			$data['page']					= 'withdrawal_status';
			$data['merchant'] 				= $merchant;
			$data['withdrawal'] 			= $withdrawal;
			$data['balance']				= $this->generalModel->merchantWalletBalance($merchant->id);

			$body 						= $this->load->view('emails/template', $data, TRUE);

			Notification::sendEmail($merchant->merchant_email, 'Your Withdrawal request on Ogaranya has been approved', $body);

			$this->session->set_flashdata(SUCCESS, 'Withdrawal Request has been succesfully approved. Email has been sent to merchant');

			logAdminActivity('Withdrawal request ['.c($withdrawal->amount).'] by ['.$merchant->merchant_name.'] has been approved');

			redirect('admin/withdrawals');
		}

		public function parent_merchants()
		{
			$data['title'] 		= 'parent merchants';
			$data['page']			= 'admin/merchants/parent_manage';
			$data['merchants']		= DB::get(MERCHANTS, 'merchant_name', 'DESC', 0, 0, ['is_parent'=>1]);
		
			$this->load->view('template', $data);
		}

		public function agents()
		{
			$data['title'] 		= 'Agents';
			$data['page']			= 'admin/agents/index';
			$data['agents']		= DB::get('agents', 'agent_name', 'DESC', 0, 0);

			$this->load->view('template', $data);
		}

		public function agent_impersonate($id = 0)
		{
			$agent = DB::first('agents', ['id' => $id]);

			logAdminActivity($agent->agent_name.' Impersonated.');

			$this->session->set_userdata('logged_in_admin', s('admin_id'));
			unset($_SESSION['admin_id']);

			$this->session->set_userdata('agent_id', $agent->id);
			$this->session->set_userdata('agent_impersonate', $agent->id);
			$this->session->set_userdata('impersonate', 'You are Currently Impersonating '.$agent->agent_name);
			
			redirect('agent');
		}

		public function agent_password($id = 0)
		{
			$agent = DB::firstOrNew('agents', ['id' => $id]);

			if(!$agent) redirect('admin/agents');

			if($this->input->post()){


				$this->form_validation->set_rules('password', 'Password', 'required|alpha_numeric_spaces');

				if($this->form_validation->run()){

					$password = $this->input->post('password');
					$password_confirmation = $this->input->post('password_confirmation');

					if($password !== $password_confirmation){

						$this->session->set_flashdata(ERROR, 'Passwords do not match');
						redirect('admin/agent_password/'.$agent->id);
					}

					DB::update('agents', ['id' => $agent->id], ['password' => hash('sha512', $password)]);

					$this->session->set_flashdata(SUCCESS, 'Password changed succesfully');

					logAdminActivity('Agent Password changed :['.$agent->agent_name.']');

					redirect('admin/agents');
					
				}
			}

			$data['title'] 			= 'Change Password for '.$agent->agent_name;
			$data['page']			= 'admin/agents/password';
			$data['agent']			= $agent;

			$this->load->view('template', $data);
		}

	}