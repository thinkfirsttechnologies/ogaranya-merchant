<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        if (!Auth::isLoggedIn())
            Auth::bounce($this->uri->uri_string());
            $this->load->model('generalModel');
            $this->load->helpers('dt_helper');
            $this->load->helpers('general_helper');
            //$this->output->enable_profiler(TRUE);
    }


    public function index()
    {


        $page_data['title'] = 'transactions';
        $page_data['page'] = 'orders/manage';
        $page_data['products'] = DT::mproducts();
        $this->load->view('template', $page_data);
    }


    function datatable_json()
    {
        $data = array();

        $Records = $this->generalModel->getMerchantOrderJson($where = [], $merchantid = s('merchant_id'));

        $button = '';

        $v = 1;

        foreach ($Records['data'] as $r) {

            $button = form_open("orders/order/" . md5(time())) .
                '<input type="hidden" name="order_id" value="' . $r['order_id'] . '">
                            <input type="submit" class="btn btn-info btn-xs btn-outline" value="View Details">' .
                form_close();

            $data [] = array(
                $v,
                $r['date_ordered'],
                $r['order_reference'],
                $r['msisdn'],
                c($r['total']),
                badge($r['order_status']),
                d($r['payment_date']),

                $button,
                $r['order_id']

            );
            $v++;

        }

        $Records['data'] = $data;

        echo json_encode($Records);
    }


    public function order()
    {


        if ($this->input->post()) {
            $order_id = $this->input->post('order_id');

            if ($order = $this->generalModel->getSingleMerchantOrder($order_id)) {

                $page_data['order'] = $order;
                $page_data['title'] = 'Order details for ' . $order->name . ' (' . $order->msisdn . ')';
                $page_data['page'] = 'orders/single';

                $this->load->view('template', $page_data);

            } else {

                redirect(site_url());
            }
        }

    }


}