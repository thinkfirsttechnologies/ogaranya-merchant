<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agent extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		if(!Auth::isLoggedIn())
			Auth::bounce($this->uri->uri_string());
	}

	public function index()
	{
		$agent 					= DB::firstOrNew('agents', ['id' => s('agent_id')]);

		$data['title']			= $agent->agent_name;
		$data['page']			= 'agent/dashboard/index';
		$data['count']			= DB::count('digital_order_fulfilment', ['agent_id' => $agent->id]);
		$data['total']			= DB::tableSum('digital_order_fulfilment', 'amount', ['agent_id' => $agent->id, 'status' => 'completed']);
		$data['completed']		= DB::count('digital_order_fulfilment', ['agent_id' => $agent->id, 'status' => 'completed']);
		$data['balance']		= (float)DB::firstOrNew('agents', ['id' => $agent->id])->wallet;
		$data['transactions']	= DB::get('digital_order_fulfilment', 'created_at', 'desc', 20, 0, ['agent_id' => $agent->id, 'status' => 'completed']);
		
		$this->load->view('template', $data);
	}

	public function transactions()
	{
		$agent 					= DB::firstOrNew('agents', ['id' => s('agent_id')]);

		$data['title'] 			= 'Transactions by '.$agent->agent_name;
		$data['page']			= 'agent/transactions/index';
		$data['transactions']	= DB::get('digital_order_fulfilment', 'created_at', 'desc', 0, 0, ['agent_id' => $agent->id]);
		
		$this->load->view('template', $data);
	}

	public function transaction($id = 0)
	{
		$transaction = DB::first('digital_order_fulfilment', ['id' => $id]);

		if(!$transaction)
			redirect('agent/transactions');

		$order = DB::firstOrNew('orders', ['order_id' => $transaction->order_id]);

		$data['transaction'] 	= $transaction;
		$data['order']			= $order; 	
		$data['customer']		= DB::firstOrNew('customer', ['msisdn' => $order->msisdn]);
		$data['title'] 			= 'Transaction details for '.$transaction->msisdn;
		$data['page']			= 'agent/transactions/transaction';

		$this->load->view('template', $data);
	}

	public function return_to_admin()
	{
		$adminId 	= s('logged_in_admin');

		unset($_SESSION['logged_in_admin'], $_SESSION['agent_id'], $_SESSION['impersonate']);

		$this->session->set_userdata('admin_id', $adminId);

		redirect('admin');
	}

}