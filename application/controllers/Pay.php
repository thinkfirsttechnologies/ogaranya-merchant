<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pay extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index($country = 'ng')
    {
        $this->form_validation->set_rules('order_id', 'Order ID', 'required|numeric');  

        if($this->form_validation->run('pay/' . $country)){

            $order_id       = $this->input->post('order_id');

            $client = new \GuzzleHttp\Client([

                'headers' => [

                    'token'         => getenv('API_TOKEN'),
                    'publickey'     => hash('sha512', getenv('API_TOKEN').getenv('API_PRIVATE_KEY'))
                ]
            ]);

            $endpoint = getenv('API_ENDPOINT').'/order/'.$order_id.'/info/NG';

            try{

                $response = $client->request('GET', $endpoint);

                if($response->getStatusCode() != 200){

                    $this->session->set_flashdata('error', 'An error occured. Please try again');

                    redirect('pay/' . $country);
                }

                $order = json_decode($response->getBody());

                if($order->status != 'success'){

                    $this->session->set_flashdata('error', $order->message);

                    redirect('pay/' . $country);

                }else{

                    $this->load->helper('string');

                    $trans_ref = random_string('alnum', 10);

                    DB::updateOrCreate(ONLINE_PAYMENT, [

                        'service_provider'      => 'Konga Pay',
                        'order_id'              => $order_id,
                        'msisdn'                => $order->msisdn,
                        'trans_ref'             => $trans_ref,
                        'amount'                => $order->amount,
                        'status'                => 'pending',
                        'created_at'            => date('Y-m-d H:i:s')

                    ], ['order_id' => $order_id]);

                    $data['country']            = $country;
                    $data['order']              = $order;
                    $data['order_id']           = $order_id;
                    $data['msisdn']             = $order->msisdn;
                    $data['trans_ref']          = $trans_ref;
                    $data['amount']             = ($order->amount * 100);
                    $data['merchant_id']        = getenv('KONGA_MERCHANT_ID');
                    $data['hash']               = hash('sha512', $data['amount'].'|'.getenv('KONGA_PUBLIC_KEY').'|'.$trans_ref);                 
                    $data['page']               = 'pay/order';

                    $this->load->view('pay/template', $data);
                }


            }catch(Exception $e){

                $this->session->set_flashdata('error', 'An error occured. Please try again');

                redirect('pay/' . $country);
            }

        }else{

            $this->load->view('pay/template', ['page' => 'pay/form', 'country' => $country]);
        }

    }

    public function requery($trans_ref = '')
    {        
        try{

            $endpoint = 'https://ignite.kongapay.com/v1/payment/requery/'.$trans_ref;

            $client = new \GuzzleHttp\Client([

                'headers' => ['Authorization'  => 'Bearer '.getenv('KONGA_PRIVATE_KEY') ]
            ]);

            $response           = $client->request('GET', $endpoint);
            $response_data      = json_decode($response->getBody());

            if($response->getStatusCode() != 200 || $response_data->resp != 0 || $response_data->data->charge->status != 'completed'){

                $data['page']       = 'pay/payment-response';
                $data['error']      = 'Transaction Failed. Please Try again or call 0808 243 1008 for assistance.';

                $this->load->view('pay/template', $data);

            }else{

                DB::update(ONLINE_PAYMENT, ['trans_ref' => $trans_ref], [

                    'payment_ref'   => $response_data->data->charge->identifier,
                    'status'        => $response_data->desc,
                    'log'           => json_encode($response_data)
                ]);

                $this->confirm_order($trans_ref);
            }

        }catch(Exception $e){

            $data['page']       = 'pay/payment-response';
            $data['error']      = 'We could not confirm your payment. Please call 0808 243 1008';

            $this->load->view('pay/template', $data);
        }


        
    }

    public function confirm_order($trans_ref = '')
    {
        try{

            $transaction = DB::first(ONLINE_PAYMENT, ['trans_ref' => $trans_ref]);

            $endpoint = getenv('API_ENDPOINT').'/order/'.$transaction->order_id.'/payment/info';

            $client = new \GuzzleHttp\Client([

                'headers' => [

                    'token'     => getenv('API_TOKEN'),
                    'publickey' => hash('sha512', getenv('API_TOKEN').getenv('API_PRIVATE_KEY'))
                ]              
            ]); 

            $response = $client->request('POST', $endpoint, [

                'json' =>  [

                    'amount'        => $transaction->amount,
                    'order_id'      => $transaction->order_id,
                    'trans_ref'     => $transaction->payment_ref,
                    'status_ref'    => '00'
                ]

            ]);

            $response = json_decode($response->getBody()->getContents());

            if($response->status == 'success'){

                $data['success'] = 'Your Payment was successful. <br> Thank you for using Ogaranya.';

            }else{

                $data['error'] = $response->message;
            }

            $data['page']   = 'pay/payment-response';

            $this->load->view('pay/template', $data);

        }catch(Exception $e){

            $data['error']  = 'We could not confirm your payment. Please call 0808 243 1008';
            $data['page']   = 'pay/payment-response';

            $this->load->view('pay/template', $data);
        }
    }
}