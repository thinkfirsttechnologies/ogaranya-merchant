<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		if(!Auth::isLoggedIn())
			Auth::bounce($this->uri->uri_string());
            $this->load->helpers('dt_helper');
            $this->load->helpers('general_helper');
	}

	public function index()
	{

		$page_data['title'] 		= 'customers';
		$page_data['page']			= 'customers/manage';
		//$page_data['customers']		= $this->generalModel->getMerchantCustomers();
		//print_r($page_data['customers']); exit;
        $page_data['products'] = DT::customer();
		$this->load->view('template', $page_data);
	}

    function customer_json()
    {
        $data = array();

        $Records = $this->generalModel->getMerchantCustomersJson();

        $button = '';

        $v = 1;

        foreach ($Records['data'] as $r) {


            $button = '<a href=" '.site_url("customers/transactions/".$r['msisdn']).'" 
          class="btn btn-info btn-xs btn-outline">View Transaction Details</a>
        ';

            $data [] = array(
                $v,
                $r['name'],
                $r['msisdn'],
                $button


            );
            $v++;

        }

        $Records['data'] = $data;

        echo json_encode($Records);
    }


    public function transactions($customerMsisdn)
	{
		$page_data['title']		= 'Orders by '.$customerMsisdn;
		$page_data['page']		= 'customers/transaction';
		$page_data['msisdn']	=  $customerMsisdn;
        $page_data['products']  = DT::customerTransaction($customerMsisdn);
		$this->load->view('template', $page_data);
	}



    function transaction($customerMsisdn)
    {
        $data = array();

        $Records = $this->generalModel->getMerchantOrderJson([ORDERS.'.msisdn'=>$customerMsisdn]);


        $button = '';

        $v = 1;

        foreach ($Records['data'] as $r) {

            $button = form_open("orders/order/" . md5(time())) .
                '<input type="hidden" name="order_id" value="' . $r['order_id'] . '">
                            <input type="submit" class="btn btn-info btn-xs btn-outline" value="View Details">' .
                form_close();

            $data [] = array(
                $v,
                $r['date_ordered'],
                $r['order_reference'],
                $r['msisdn'],
                c($r['total']),
                badge($r['order_status']),
                d($r['payment_date']),

                $button,
                $r['order_id']

            );
            $v++;

        }

        $Records['data'] = $data;

        echo json_encode($Records);
    }

}