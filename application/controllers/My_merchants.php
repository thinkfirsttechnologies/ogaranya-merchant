<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_merchants extends CI_Controller {


    public function __construct()
    {
        parent::__construct();
        if(!Auth::isLoggedIn())
            Auth::bounce($this->uri->uri_string());
        $this->load->model('generalModel');
        $this->load->helpers('dt_helper');
        $this->load->helpers('general_helper');
        //$this->output->enable_profiler(TRUE);
    }

    public function index()
    {

        $page_data['title'] 		= 'My Merchants';
        $page_data['page']			= 'my_merchants/manage';
        $page_data['products']      = DT::myMerchant();

        $this->load->view('template', $page_data);
    }



    function datatable_json()
    {
        $data = array();


        $Records = $this->generalModel->getMyMerchantJson();

        $button = '';

        $v = 1;

        foreach ($Records['data'] as $r) {

            $button = form_open("my_merchants/merchant_orders/" . md5(time())) .
                '<input type="hidden" name="id" value="' . $r['id'] . '">
                  <input type="hidden" name="name" value="'.$r['merchant_name'] .'">
                            <input type="submit" class="btn btn-info btn-xs btn-outline" value="View transactions">' .
                form_close();


            $data [] = array(
                $v,
                $r['merchant_name'],
                $r['merchant_phone'],
                $r['merchant_email'],
                $r['merchant_contact_person'],
                $r['wallet'],
                $button,
                $r['id']

            );
            $v++;

        }

        $Records['data'] = $data;

        echo json_encode($Records);
    }



    public function merchant_orders()
    {
        if($this->input->post()){


            $id = $this->input->post('id');
            $page_data['name']          = $this->input->post('name');
            $page_data['title'] 		= 'transactions';
            $page_data['page']			= 'my_merchants/orders';
            $page_data['products']		= DT::myMerchantOrder($id);


            $this->load->view('template', $page_data);

        }else{

            redirect(site_url());
        }


    }

    function m_json($merchantId)
    {
        $data = array();

        $Records = $this->generalModel->getMerchantOrderJson($where =[], $merchantId);


        $button = '';

        $v = 1;

        foreach ($Records['data'] as $r) {

            $button = form_open("my_merchants/order/" . md5(time())) .
                '<input type="hidden" name="order_id" value="' . $r['order_id'] . '">
                            <input type="submit" class="btn btn-info btn-xs btn-outline" value="View Details">' .
                form_close();

            $data [] = array(
                $v,
                $r['date_ordered'],
                $r['order_reference'],
                $r['msisdn'],
                c($r['total']),
                badge($r['order_status']),
                d($r['payment_date']),
                $button,
                $r['order_id']

            );
            $v++;
        }

        $Records['data'] = $data;

        echo json_encode($Records);
    }




    function merchantOrders_json($id)
    {
        $data = array();

        $Records = $this->generalModel->getMerchantOrderJson($where =[], $id);

        $button = '';

        $v = 1;

        foreach ($Records['data'] as $r) {

            $button = form_open("my_merchants/order/" . md5(time())) .
                '<input type="hidden" name="order_id" value="' . $r['order_id'] . '">
                            <input type="submit" class="btn btn-info btn-xs btn-outline" value="View Details">' .
                form_close();

            $data [] = array(
                $v,
                $r['date_ordered'],
                $r['order_reference'],
                $r['msisdn'],
                c($r['total']),
                badge($r['order_status']),
                d($r['payment_date']),
                $button

            );
            $v++;
        }

        $Records['data'] = $data;

        echo json_encode($Records);
    }




    public function order()
    {


        if($this->input->post())
        {
            $order_id = $this->input->post('order_id');
            $_val = 1;
            if($order = $this->generalModel->getSingleMerchantOrder($order_id,  $where = [] , $_val)){

                $page_data['order'] 		= $order;
                $page_data['title'] 		= 'Order details for '.$order->name.' ('.$order->msisdn.')';
                $page_data['page']			= 'my_merchants/single';

                $this->load->view('template', $page_data);

            }else{

                redirect(site_url());
            }
        }

    }

}