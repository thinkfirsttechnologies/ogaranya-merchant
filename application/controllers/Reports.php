<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		if(!Auth::isLoggedIn())
			Auth::bounce($this->uri->uri_string());
	}

	public function index()
	{

		$page_data['title'] 		= 'reports';
		$page_data['page']			= 'reports/manage';
		$this->load->view('template', $page_data);
	}
}