<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Authentication extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->helper('recaptcha');

    }

    public function index()
    {

        if(Auth::isLoggedIn()) 
            redirect(site_url('home'));

        $page_data['page']      = 'auth/login';
        $page_data['title']     = 'Merchant Login';


        if ($this->input->post()) {

            $email      = $this->input->post('email');
            $password   = hash('sha512', $this->input->post('password'));

            $data = ['merchant_email' => $email, 'password' =>  $password];
            if(Auth::login($data))
                redirect('home');


            $data = ['agent_email' => $email, 'password' => $password];
            if(Auth::loginAgent($data)) 
                redirect('agent');

            $data = ['email' => $email, 'password'  => $password];
            if(Auth::loginAdmin($data) == true)
                redirect('admin');

            $this->session->set_flashdata(MESSAGE, '<p>Invalid Username or Password</p>');        }

            $this->load->view('auth/template', $page_data);
        }
        
        public function logout()
        {
            Auth::logout();

            redirect(site_url('authentication'));

        }

        public function code()
        {

            if(!$this->input->post('code')){

                $this->session->set_flashdata('error', 'Registration Code is required');

                redirect(site_url());
            }

            $code = DB::first(CODES, ['code'=>$this->input->post('code')]);

            if(is_null($code)){

                $this->session->set_flashdata('error', 'Invalid Registration Code');

                redirect(site_url());
            }

            if($code->status == USED){

                $this->session->set_flashdata('error', 'Registration Code have been used.');

                redirect(site_url());

            }


            if($code->status == STARTED){

                if($code->end_date <= date('Y-m-d H:i:s')){

                    $this->session->set_flashdata('error', 'Sorry, this invitation code Has become invalid due to inactivity.');

                    redirect(site_url());

                }

                redirect('registration/register/'.$code->hash);
            }

            $data['status']             = STARTED;
            $data['start_date']         = date('Y-m-d H:i:s');
            $data['end_date']           = date('Y-m-d H:i:s', strtotime($data['start_date'].' + 2 days'));

            $response = DB::update(CODES, ['code_id' => $code->code_id], $data);

            redirect('registration/register/'.$code->hash);


        }

        public function forgot_password()
        {
            if($this->input->post()){

                $email = $this->input->post('email');

                $merchant = DB::first(MERCHANTS, ['merchant_email' =>$email]);

                if($merchant == null){

                    $this->session->set_flashdata(MESSAGE, 'We could not find an account with '.$email);

                }else{

                    $email_data['page']             = 'forgot_password';
                    $email_data['merchant']         = $merchant;

                    $body       = $this->load->view('emails/template', $email_data, true);
                    $subject    = 'Password Reset on Ogaranya';

                    Notification::sendEmail($merchant->merchant_email, $subject, $body);
                }

            }

            $data['page']      = 'auth/forgot_password';
            $data['title']     = 'Forgot Password';

            $this->load->view('auth/template', $data);
        }

        public function reset_password($code = '')
        {

           $merchant = DB::first(MERCHANTS, ['password_reset' => $code]);

           if($merchant == null){

            redirect(site_url());
        }

        $data['code']      = $code;
        $data['page']      = 'auth/reset_password';
        $data['title']     = 'Reset Password';
        $data['hide_form'] = 0;

        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|matches[password]');

        if($this->form_validation->run()){

            DB::update(MERCHANTS, ['id' => $merchant->id], [

                'password'          => hash('sha512', $this->input->post('password')),
                'password_reset'    => substr(hash('sha512', time()), 0, 15)

            ]);

            $email_data['page']             = 'password_changed';
            $email_data['merchant']         = $merchant;

            $body       = $this->load->view('emails/template', $email_data, true);
            $subject    = 'Password Changed on Ogaranya';

            Notification::sendEmail($merchant->merchant_email, $subject, $body);

            $this->session->set_flashdata(MESSAGE, 'Your Password has been reset succesfully.');

            $data['hide_form'] = 1;

        }

        $this->load->view('auth/template', $data);

    }

}
