<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Developer extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!Auth::isLoggedIn())
            Auth::bounce($this->uri->uri_string());
        $this->load->helpers('db_helper');
        $this->load->helpers('general_helper');

    }


    public function live()
    {

        $page_data['title'] = 'Developer';
        $page_data['page'] = 'developer/production';
        $page_data['header'] = 'Api keys';
        $page_data['scripts'] = 'developer/script';
        $page_data['mid'] = DB::getCell(MERCHANT, ['id' => s('merchant_id')], 'merchant_phone');
        $page_data['api'] = DB::getRow(API_AGENTS, ['merchant_id' => s('merchant_id')]);
        $page_data['api'] = api('live', $page_data['mid']);
        $this->load->view('template', $page_data);
        
    }

    public function test()
    {
        $page_data['title'] = 'Developer';
        $page_data['page'] = 'developer/staging';
        $page_data['header'] = 'Api keys';
        $page_data['scripts'] = 'developer/script';
        $page_data['mid'] = DB::getCell(MERCHANT, ['id' => s('merchant_id')], 'merchant_phone');
        $page_data['api'] = DB::getRow(API_AGENTS, ['merchant_id' => s('merchant_id')]);
        $page_data['api'] = api('test', $page_data['mid']);
        $this->load->view('template', $page_data);


    }

    public function getApi($type = '')
    {

        $pag = '';

        if ($type == 'test') {

            $pag = 'staging';

        } else if ($type == 'live') {

            $pag = 'production';
        }
        $page_data['title'] = 'Developer';
        $page_data['page'] = 'developer/' . $pag;
        $page_data['header'] = 'Api keys';
        $page_data['scripts'] = 'developer/script';
        $page_data['mid'] = DB::getCell(MERCHANT, ['id' => s('merchant_id')], 'merchant_phone');
        $merchant = DB::getRow(MERCHANT, ['id' => s('merchant_id')]);
        $country = DB::getCell(COUNTRIES, ['id' => $merchant->country_id], 'country_code');
        $page_data['api'] = generateApi($type, $merchant->merchant_phone, $country);
        $this->load->view('template', $page_data);

    }


}


