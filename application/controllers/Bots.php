<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bots extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function slack()
	{
		$error = $success = '';

		if($this->input->get('code')){

			$code = $this->input->get('code');

			$client = new \GuzzleHttp\Client([

				'headers' => [

					'token'         => getenv('API_TOKEN'),
					'publickey'     => hash('sha512', getenv('API_TOKEN').getenv('API_PRIVATE_KEY'))
				]
			]);

			$endpoint = getenv('API_ENDPOINT').'/slack/auth/'.$code;

			try{

				$response = $client->request('GET', $endpoint);

				$responseData = json_decode($response->getBody());

				if($response->getStatusCode() != 200 || $responseData->status != 'success'){

					$error = 'An error occured. Please try again.';

				}else{

					$success =  'Slack Authorization was successful. Welcome to Ogaranya!';
				}

			}catch(Exception $e){

				$error = 'An error occured. Please try again.';
			}
		}
		
		$data['error'] 		= $error;
		$data['success']	= $success;
		$data['page']		= 'bots/slack';

		$this->load->view('pay/template', $data);
	}
}