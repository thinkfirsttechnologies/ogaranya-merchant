<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!Auth::isLoggedIn())
				Auth::bounce($this->uri->uri_string());

		if($this->session->has_userdata('agent_id'))
			return redirect('agent');
		
			//$this->enable_profiler(TRUE);
	}

	public function index()
	{

		$page_data['scripts'] 			= 'dashboard/scripts';
		$page_data['title']				= 'dashboard';
		$page_data['page']				= 'dashboard/index';
		
		$page_data['total_orders']		= $this->generalModel->merchantOrdersCount();
		$page_data['total_orders_amount']= $this->generalModel->merchantOrdersAmount();
		$page_data['wallet_balance']	= $this->generalModel->merchantGrandBalance();
		$page_data['total_products']	= DB::count(PRODUCTS, ['merchant_id'=>s('merchant_id')]);
		$page_data['highest_order']		= $this->generalModel->highestMerchantOrder();
		$page_data['user_activity']		= DB::get(MERCHANT_ACTIVITIES, 'activity_id', 'DESC', 5, 0, ['merchant_id'=>s('merchant_id')]);
		$page_data['recent_orders']		= $this->generalModel->getMerchantOrders([ORDERS.'.order_status'=>COMPLETED], 0, 'order_id', 'DESC', 5);

		$this->load->view('template', $page_data);
	}

	public function return_to_admin()
	{
		$adminId 	= s('logged_in_admin');

		unset($_SESSION['logged_in_admin'], $_SESSION['merchant_id'], $_SESSION['impersonate']);

		$this->session->set_userdata('admin_id', $adminId);

		redirect('admin');
	}

}