<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wallet extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		
		if(!Auth::isLoggedIn())
			Auth::bounce($this->uri->uri_string());
            $this->load->helpers('dt_helper');
            $this->load->helpers('general_helper');
		}

		public function index()
		{

			$page_data['title'] 		= 'wallet';
			$page_data['page']			= 'wallet/manage';
			$page_data['scripts'] 		= 'wallet/scripts';
            $page_data['activities']    = $this->generalModel->getMerchantWalletActivities();

			$this->load->view('template', $page_data);
		}


    public function request()
		{
			if(!$this->input->post())
				redirect(site_url('wallet'));

			$data 			= $this->input->post();
			$maximumAmount 	= $this->generalModel->merchantWalletBalance();

			$this->form_validation->set_rules('amount', 'Amount', 'required|less_than_equal_to['.$maximumAmount.']');
			$this->form_validation->set_rules('securit_answer', 'Security Answer', 'required');

			if($this->form_validation->run()){

				$data['security_answer'] = strtolower($data['security_answer']);
				$merchantAccountDetails = DB::getRow(MERCHANTS_ACCOUNT_DETAILS, ['merchant_id'=>s('merchant_id')]);

				if(trim($data['security_answer']) != trim($merchantAccountDetails->security_answer)){

					$this->session->set_flashdata(ERROR, 'Security Answer Invalid, Please provide the correct answer.');
					redirect('wallet');

				}else if($data['amount'] < MIN_WITHDRAWAL_VALUE){

					$this->session->set_flashdata(ERROR, 'Minimum Withdrawal amount is 2000.00');
					redirect('wallet');
				}

				$data['merchant_id'] 	= s('merchant_id');
				$data['created_at']	 	= date('Y-m-d H:i:s');
				unset($data['security_answer']);

				DB::save(MERCHANT_WITHDRAWALS, $data);

				$merchant = DB::get(MERCHANTS, ['id'=>s('merchant_id')]);

				$email['merchant']		= $merchant;
				$email['page']			= 'merchant_withdrawal';

				$body 					= $this->load->view('emails/template', $email, true);

				Notification::sendEmail($merchant->merchant_email, 'Withdrawal Request Notification', $body);

				$this->session->set_flashdata(SUCCESS, $message);

				logActivity('Withdrawal request made. [value: '.$data['amount'].']');

				redirect('wallet');

			}else{

				$this->index();
			}
		}
	}