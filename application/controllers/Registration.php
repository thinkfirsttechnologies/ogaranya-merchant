<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Registration extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->helper('recaptcha');
    }

    public function register($codeHash = '')
    {

        if ($codeHash == '')
            redirect(site_url());

        $code   = DB::first(CODES, ['hash' => $codeHash]);

        if ($code == null || $code->status == USED)
            redirect(site_url());

        if ($code->status == STARTED && $code->end_date <= date('Y-m-d H:i:s'))
            redirect(site_url());

        $recaptcha = new Recaptcha();

        $page_data['code']          = $code;
        $page_data['widget']        = $recaptcha->getWidget();
        $page_data['script']        = $recaptcha->getScriptTag();
        $page_data['page']          = 'auth/register';
        $page_data['title']         = 'Registration';


        if ($this->input->post()) {

            $data = $this->input->post();


            if (!isset($data['g-recaptcha-response'])) {

                $this->session->set_flashdata('error', 'Please confirm you are human');

                redirect('registration/register/' . $codeHash);
            }

            $response = $recaptcha->verifyResponse($data['g-recaptcha-response']);

            if (!isset($response['success']) || $response['success'] == false) {

                $this->session->set_flashdata('error', 'Please confirm you are human');

                redirect('registration/register/' . $codeHash);
            }

            $this->form_validation->set_rules('merchant_name', 'Store Name', 'required|alpha_numeric_spaces|is_unique[' . MERCHANTS . '.merchant_name]');
            $this->form_validation->set_rules('merchant_email', 'Email', 'required|valid_email|is_unique[' . MERCHANTS . '.merchant_email]');
            $this->form_validation->set_rules('merchant_address', 'Address', 'required');
            $this->form_validation->set_rules('merchant_contact_person', 'Contact Person', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
            $this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|min_length[6]|matches[password]');

            if ($this->form_validation->run('register')) {

                $country_prefix          = DB::first(COUNTRIES, ['id' => $code->country_id])->country_prefix;

                $data['merchant_phone']  = $code->msisdn;
                $data['status']          = 'disabled';
                $data['password']        = hash('sha512', $data['password']);
                $data['password_reset']  = substr(hash('sha512', time()), 0, 15);
                $data['country_id'] = $code->country_id;

                $data['date_added']      = date('Y-m-d H:i:s');

                unset($data['cpassword'], $data['g-recaptcha-response']);

                $insert_id  = DB::save(MERCHANTS, $data);

                $email['page']             = 'verify_email';
                $email['merchant']         = DB::first(MERCHANTS, ['id' => $insert_id]);

                $body       = $this->load->view('emails/template', $email, true);
                $subject    = 'We got your registration request.';
                $file       =   'pdfs/Forms.pdf';

                Notification::sendEmail($data['merchant_email'], $subject, $body, $file);

                DB::update(CODES, ['code' => $code->code], ['status' => USED, 'used_by' => $insert_id]);

                redirect('registration/success');
            }
        }

        $this->load->view('auth/template', $page_data);
    }

    
    public function success()
    {
        $page_data['page']  = 'auth/success';
        $page_data['title'] = 'Registration Succesful';

        $this->load->view('auth/template', $page_data);
    }

    public function verify_email($hash = '')
    {

        $merchant = DB::first(MERCHANTS, ['password_reset' => $hash]);

        if ($merchant == null)
            redirect(site_url());

        if ($merchant->email_verified == 1)
            redirect(site_url());

        DB::update(MERCHANTS, ['id' => $merchant->id], ['email_verified' => 1]);

        $email['page']      = 'new_registration';
        $email['merchant']  = $merchant;

        $body       = $this->load->view('emails/template', $email, true);
        $subject    = 'Welcome to Ogaranya.com, ' . $merchant->merchant_name;

        Notification::sendEmail($merchant->merchant_email, $subject, $body);

        $this->session->set_flashdata('success', 
        'Your Email has been Verified Successfully. We have also sent you an onboarding email. Please call 0808 243 1008 if you have any questions.');

        redirect('authentication');
    }
}
