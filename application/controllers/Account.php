<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		if(!Auth::isLoggedIn())
			Auth::bounce($this->uri->uri_string());
		}

		public function index()
		{

			$page_data['title'] 		= 'account';
			$page_data['page']			= 'account/manage';
			$page_data['merchant']		= DB::first(MERCHANTS, ['id' => s('merchant_id')]);
			$page_data['has_details']	= false;

			$account_details 			= DB::firstOrNew(MERCHANTS_ACCOUNT_DETAILS, ['member_id' => s('merchant_id')]);

			if($account_details->account_number != ''){

				$page_data['account_details'] 	= $account_details;
				$page_data['page']				= 'account/manage';

			}else{

				$page_data['page']		= 'account/form';
			}

			$this->load->view('template', $page_data);
		}

		public function save()
		{
			if(!$this->input->post())
				redirect(site_url('account'));

			$data = $this->input->post();

			$this->form_validation->set_rules('bank_name', 'Bank Name', 'required|alpha_numeric_spaces');	
			$this->form_validation->set_rules('account_number', 'Account Number', 'required|exact_length[10]|callback_validate_account_number');
			$this->form_validation->set_rules('account_name', 'Account Name', 'required');	
			$this->form_validation->set_rules('security_question', 'Security Question', 'required');	
			$this->form_validation->set_rules('security_answer', 'Security Answer', 'required');

			if($this->form_validation->run('account')){

				$data['security_answer'] 	= trim(strtolower($data['security_answer']));
				$data['member_id'] 		= s('merchant_id');
				$data['created_at']	 		= date('Y-m-d H:i:s');

				unset($data['country_id']);

				DB::save(MERCHANTS_ACCOUNT_DETAILS, $data);

				$merchant = DB::first(MERCHANTS, ['id' => s('merchant_id')]);

				$email['merchant']		= $merchant;
				$email['page']			= 'merchant_account_update';
				$body 					= $this->load->view('emails/template', $email, true);
				//print_r($merchant->merchant_email); exit;

				Notification::sendEmail($merchant->merchant_email, 'Bank Account Information Updated', $body);

				logActivity('Bank Account Information updated.');

				$this->session->set_flashdata(SUCCESS, l('action_succesful'));

				redirect('account');

			}

			$this->index();

		}

		function validate_account_number($input)
		{
			if($this->input->post('country_id') != 1)
				return true;

			$code = DB::firstOrNew(BANKS, ['name' => $this->input->post('bank_name')])->code;

			if(strlen($input) != 10 || !isNubanValid($code.$input)){

				$this->form_validation->set_message('validate_account_number', 'Your account number is invalid, please check and try again!');

				return false;
			}

			return true;
		}

		public function sort_code()
		{
			echo DB::firstOrNew(BANKS, ['name' => $this->input->post('bank_name')])->sort_code;
		}
	}